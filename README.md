<!-- # Canvas / Discordgroup number :10 am Group 12

# Names of the team members: Anish Roy, Avi Ghayalod, Isaac Adams, Mya Mahaley, Anthony Chhang

# Name of the project: Find-a-Home

# URL of the GitLab repo

```
https://gitlab.com/aghayalod/find-a-home
```
# The Proposed Project

```
FindaHome is a project based around helping individuals looking for asylum or as
refugees find information about the countries they are considering moving to. This
includes social welfare information as well as general information regarding the country’s
asylum status.
```
# API URLs

```
https://api.unhcr.org/docs/index.html
```
```
https://www.globalgiving.org/api/methods/get-all-projects-for-a-country/
```
```
https://www.numbeo.com/api/doc.jsp
```
```
https://data.oecd.org/api/sdmx-json-documentation/,
https://stats.oecd.org/index.aspx?DataSetCode=BLI
```
```
```
```
https://documenter.getpostman.com/view/1134062/T1LJjU52

https://mediastack.com/documentation
```
# Charity

Searchable Attributes:

1. Name
2. Country/Region
3. Theme
4. Address
5. Activities

Sortable Attributes:

1. Status
2. Total projects
3. Funding
4. Goal
5. Approval Date



Connections to other Models:

1. Charity -> News: Charities are set up to address big issues that appear in the news.
2. Charity -> Country: Charities provide assistance for people trying to immigrate

Rich Media:

1. Logo
2. Website URL

Instances: 400

# News

Searchable Attributes:
1. Source
2. Title
3. Description
4. URL
5. Author

Filterable/Sortable Attributes:
1. Country
2. Published At
3. Language
4. Category
5. Charities

Connections to other Models:
1. News -> Country: Look at the big news headlines that are happening in a country.
2. News -> Charities: News will have charities focused on that specific issue or area.

Rich Media:
1. URL to Image
2. Twitter Link to Publisher

Instances: 300

# Country

Searchable Attributes:

1. Country Name
2. Language
3. Region
4. Population
5. Currency

Filterable/Sortable Attributes:

1. Dwellings without basic facilities
2. Household Net worth
3. Employment Rate
4. Educational Attainment
5. Life Expectancy
6. Life Satisfaction
7. Homicide Rate
8. Employees working very long hours
9. Voter Turnout

Connections to other Models:

1. Country -> Charity: Each country has charities available
2. Country -> News: Each country has news headlines.

Rich Media:

1. Flag
2. Map

Instances: 40

# 3 Questions: 1

1. How can I assist with headliner issues currently going on in specific countries?
2. What countries provide the best quality of life?
3. What countries have the best assistance for the unemployed and immigrants? -->

# Find a Home

## Team 7 10am

- Avi Ghayalod
  - EID      : akg2628
  - GitLab ID: aghayalod
- Anthony Chhang
  - EID      : adc4233
  - GitLab ID: anthony_chhang
- Anish Roy
  - EID      : ar62378
  - GitLab ID: anishroy1
- Isaac Adams
  - EID      : iga263
  - GitLab ID: filipjnybob
- Mya Mahaley
  - EID      : mjm7656
  - GitLab ID: myamahaley

## Git SHA

c2465bef3e7bed9a6b0fb91ea7a84a9c928e1d05

## Project Leader

### Responsibilities
- Assign issues and tasks to group.
- Schedule meetings.
- Approve proposal changes.
- Keep overall track of website.

### Phase Leader
- Phase 1: Avi Ghayalod
- Phase 2: Isaac Adams
- Phase 3: Anish Roy
- Phase 4: Mya Mahaley

## GitLab Pipelines

https://gitlab.com/aghayalod/find-a-home/-/pipelines

## Website Link

https://www.findahome.me

## Phase 1

### Estimated Completion Time for Each Member

- Avi Ghayalod:     12 hours
- Isaac Adams:      11 hours
- Anthony Chhang:   12 hours
- Anish Roy:        12 hours
- Mya Mahaley:      11 hours

### Actual Completion Time for Each Member

- Avi Ghayalod:     15 hours
- Isaac Adams:      16 hours
- Anthony Chhang:   16 hours
- Anish Roy:        15 hours
- Mya Mahaley:      16 hours

### Comments


## Phase 2

### Estimated Completion Time for Each Member
- Avi Ghayalod:     25 hours
- Isaac Adams:      25 hours
- Anthony Chhang:   25 hours
- Anish Roy:        25 hours
- Mya Mahaley:      25 hours

### Actual Completion Time for Each Member

- Avi Ghayalod:     43 hours
- Isaac Adams:      43 hours
- Anthony Chhang:   7 hours
- Anish Roy:        40 hours
- Mya Mahaley:      5 hours

### Comments
Unfortunately, Mya had a concussion during this phase and was under doctors instructuions to not work more than a set amount a day.


## Phase 3

### Estimated Completion Time for Each Member
- Avi Ghayalod:     30 hours
- Isaac Adams:      30 hours
- Anthony Chhang:   30 hours
- Anish Roy:        30 hours
- Mya Mahaley:      30 hours

### Actual Completion Time for Each Member
- Avi Ghayalod:     35 hours
- Isaac Adams:      34 hours
- Anthony Chhang:   30 hours
- Anish Roy:        32 hours
- Mya Mahaley:      32 hours

### Comments
- Mya: Stylistic elements based off of the TexasVotes repository. Borrowed code has been indicated throughout the project. https://gitlab.com/forbesye/fitsbits/ 


## Phase 3

### Estimated Completion Time for Each Member
- Avi Ghayalod:     5 hours
- Isaac Adams:      5 hours
- Anthony Chhang:   5 hours
- Anish Roy:        5 hours
- Mya Mahaley:      5 hours

### Actual Completion Time for Each Member
- Avi Ghayalod:     5 hours
- Isaac Adams:      5 hours
- Anthony Chhang:   3 hours
- Anish Roy:        3 hours
- Mya Mahaley:      13 hours

### Comments
- Mya: Visualizations adapted from starter code. Indicated in visualizations files
  - https://www.react-simple-maps.io/examples/map-chart-with-tooltip/
  - https://www.react-simple-maps.io/examples/world-choropleth-mapchart/
  - https://www.geeksforgeeks.org/create-a-radar-chart-using-recharts-in-reactjs/
  - https://www.geeksforgeeks.org/create-a-bar-chart-using-recharts-in-reactjs/
