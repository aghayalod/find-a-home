from crypt import methods
from Init import app, db, ma
from flask import request, jsonify
from Models import Charities, Countries, News, Theme
from Models import (
    charities_schema,
    countries_schema,
    news_schema,
    themes_schema,
    all_charities_schema,
    all_countries_schema,
    all_news_schema,
    all_themes_schema,
)
import flask_restless
from flask_cors import CORS
from countries import *
from charities import *
from news import *

# basic route to start
@app.route("/")
def basic_start():
    print("DEBUG")
    return "FIND A HOME!"


db.create_all()


# REFERENCED GROUP AROUND THE WORLD FROM LAST SEMESTER
# COUNTRIES
# Retrieve data for all countries
@app.route("/countries", methods=["GET"])
def get_all_countries():
    parameters = request.args.to_dict(flat=False)
    query = db.session.query(Countries)

    # set default values of page number and instances per page
    page = 1
    per_page = 10
    # check if values set for pagination
    if "page" in parameters:
        page = int(parameters["page"][0])
    if "per_page" in parameters:
        per_page = int(parameters["per_page"][0])
    # apply parameters
    query = filter_countries(query, parameters)
    query = sort_countries(query, parameters)
    query = search_countries(query, parameters)
    # paginate
    all_countries = query.paginate(page=page, per_page=per_page)
    print(all_countries.items)
    num_results = query.count()
    print(str(num_results))
    # TODO create schemas for all models
    result = all_countries_schema.dump(all_countries.items)
    return {
        "result": result,
        "count": num_results,
        "page_num": page,
        "total_pages": str(int(num_results / per_page)),
    }


# Retrieve data for country by id
@app.route("/countries/<id>", methods=["GET"])
def get_country(id):
    country = Countries.query.get(id)
    print("DEBUG")
    print(country)
    result = countries_schema.jsonify(country)
    return result


# CHARITIES

# Retrieve data for all charities
@app.route("/charities", methods=["GET"])
def get_all_charities():
    parameters = request.args.to_dict(flat=False)
    query = db.session.query(Charities)

    # set default values of page number and instances per page
    page = 1
    per_page = 10
    # check if values set for pagination
    if "page" in parameters:
        page = int(parameters["page"][0])
    if "per_page" in parameters:
        per_page = int(parameters["per_page"][0])
    # apply parameters
    query = filter_charities(query, parameters)
    query = sort_charities(query, parameters)
    query = search_charities(query, parameters)
    num_results = query.count()
    # paginate
    all_charities = query.paginate(page=page, per_page=per_page)
    # TODO create schemas for all models
    result = all_charities_schema.dump(all_charities.items)
    return {"result": result, "count": num_results}


# Retrieve data for charity by id
@app.route("/charities/<id>", methods=["GET"])
def get_charity(id):
    charity = Charities.query.get(id)
    result = charities_schema.dump(charity)
    return result


# NEWS

# Retrieve data for all news
@app.route("/news", methods=["GET"])
def get_all_news():
    parameters = request.args.to_dict(flat=False)
    query = db.session.query(News)

    # set default values of page number and instances per page
    page = 1
    per_page = 10
    # check if values set for pagination
    if "page" in parameters:
        page = int(parameters["page"][0])
    if "per_page" in parameters:
        per_page = int(parameters["per_page"][0])
    query = filter_news(query, parameters)
    query = sort_news(query, parameters)
    query = search_news(query, parameters)
    num_results = query.count()
    # paginate
    all_news = query.paginate(page=page, per_page=per_page)
    # TODO create schemas for all models
    result = all_news_schema.dump(all_news.items)
    return {"result": result, "count": num_results}


# Retrieve data for charity by id
@app.route("/news/<id>", methods=["GET"])
def get_news(id):
    story = News.query.get(id)
    result = news_schema.dump(story)
    return result


# if starting app run api
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=3000)
