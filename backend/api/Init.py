# flask imports
from flask import Flask
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from marshmallow import fields

# used for editing within directory
import os

# loads environment variables
from dotenv import load_dotenv

app = Flask(__name__)
# enable cors on app
CORS(app)

# load in credentials and config of app
load_dotenv()
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("AWS_DB_KEY")
app.config["SQLALCHEMY_ECHO"] = False
app.debug = True

# set to false to avoid significant overhead
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

# create database to be used later on
db = SQLAlchemy(app)

ma = Marshmallow(app)
