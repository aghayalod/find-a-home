from dataclasses import field
from pyexpat import model
from types import new_class
from unicodedata import name
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field
from marshmallow import fields, Schema
from Init import db, app, ma
import datetime

"""
Define Models of Charities, News, and Countries
"""

country_charities = db.Table(
    "country_charities",
    db.Column(
        "country_id", db.String(255), db.ForeignKey("countries.id"), primary_key=True
    ),
    db.Column(
        "charity_id", db.String(500), db.ForeignKey("charities.id"), primary_key=True
    ),
)

charity_themes = db.Table(
    "charity_themes",
    db.Column(
        "charity_id", db.String(500), db.ForeignKey("charities.id"), primary_key=True
    ),
    db.Column("theme_id", db.String(255), db.ForeignKey("themes.id"), primary_key=True),
)

charity_news = db.Table(
    "charity_news",
    db.Column(
        "charity_id", db.String(500), db.ForeignKey("charities.id"), primary_key=True
    ),
    db.Column("news_id", db.String(255), db.ForeignKey("news.id"), primary_key=True),
)


class Countries(db.Model):
    __tablename__ = "countries"
    query: db.Query  # type hint
    id = db.Column(db.String(255), primary_key=True, unique=True)
    ico2 = db.Column(db.String(2))
    name = db.Column(db.String(255))
    language = db.Column(db.String(255))
    region = db.Column(db.String(255))
    population = db.Column(db.Integer())
    currency = db.Column(db.String(45))
    lat = db.Column(db.Float)
    long = db.Column(db.Float)
    dwellings_no_facilities_rate = db.Column(db.Float())
    household_net_worth = db.Column(db.Float())
    employment_rate = db.Column(db.Float())
    education_rate = db.Column(db.Float())
    life_expectancy = db.Column(db.Float())
    life_satisfaction_rating = db.Column(db.Float())
    homicide_rate = db.Column(db.Float())
    long_hours_rate = db.Column(db.Float())
    voter_turnout = db.Column(db.Float())
    flag_img = db.Column(db.Text())
    charities = db.relationship(
        "Charities",
        secondary=country_charities,
        lazy="subquery",
        back_populates="countries",
    )
    news = db.relationship("News", backref="country", lazy=True)

    def __init__(self, id, name):
        self.id = id
        self.name = name


# TODO
class Charities(db.Model):
    __tablename__ = "charities"
    query: db.Query  # type hint
    id = db.Column(db.String(500), primary_key=True, unique=True)
    name = db.Column(db.String(1000))
    # switch from array in current postman API to single string with delimiter
    themes = db.relationship("Theme", secondary=charity_themes, lazy=False)
    address = db.Column(db.String(255))
    activities = db.Column(db.Text())
    status = db.Column(db.String(255))
    total_donations = db.Column(db.Integer())
    funding = db.Column(db.Float())
    goal = db.Column(db.Float())
    approval_date = db.Column(db.Date())
    image = db.Column(db.Text())
    url = db.Column(db.String(255))
    countries = db.relationship(
        "Countries",
        secondary=country_charities,
        lazy="subquery",
        back_populates="charities",
    )
    news = db.relationship(
        "News", secondary=charity_news, back_populates="charities", lazy=True
    )
    organization = db.Column(db.String(255))

    def __init__(self, id):
        self.id = id


class Theme(db.Model):
    __tablename__ = "themes"
    id = db.Column(db.String(255), primary_key=True, unique=True)
    name = db.Column(db.String(255))

    def __init__(self, id, name):
        self.id = id
        self.name = name


# TODO
class News(db.Model):
    __tablename__ = "news"
    query: db.Query  # type hint
    id = db.Column(db.String(255), primary_key=True, unique=True)
    source = db.Column(db.String(255))
    title = db.Column(db.String(255))
    description = db.Column(db.Text())
    url = db.Column(db.String(255))
    author = db.Column(db.String(255))
    country_id = db.Column(db.String(255), db.ForeignKey("countries.id"))
    published_at = db.Column(db.Date())
    language = db.Column(db.String(255))
    category = db.Column(db.String(255))
    image = db.Column(db.Text())
    charities = db.relationship(
        "Charities", secondary=charity_news, back_populates="news", lazy=True
    )

    def __init__(self, id):
        self.id = id


"""
Describe Schemas associated with models
"""


class newsSchema(SQLAlchemySchema):
    class Meta:
        model = News

    id = fields.Str(required=True)
    source = fields.Str(required=True)
    type = fields.Str(required=True, attribute="type_name")
    title = fields.Str(required=True)
    description = fields.Str(required=True)
    url = fields.Str(required=True)
    author = fields.Str(required=True)
    image = fields.Str(required=True)
    country_id = fields.Str(required=True)
    charities = fields.Nested(
        "charitiesSchema",
        only=(
            "id",
            "name",
            "type",
        ),
        required=True,
        many=True,
    )
    published_at = fields.Str(required=True)
    language = fields.Str(required=True)
    category = fields.Str(required=True)


class themesSchema(SQLAlchemySchema):
    class Meta:
        model = Theme

    id = fields.Str(required=True)
    name = fields.Str(required=True)
    type = fields.Str(required=True, attribute="type_name")


class charitiesSchema(SQLAlchemySchema):
    class Meta:
        model = Charities

    id = fields.Str(required=True)
    name = fields.Str(required=True)
    type = fields.Str(required=True, attribute="type_name")
    # switch from array in current postman API to single string with delimiter
    address = fields.Str(required=True)
    activities = fields.Str(required=True)
    status = fields.Str(required=True)
    total_donations = fields.Int(required=True)
    funding = fields.Float(required=True)
    goal = fields.Float(required=True)
    approval_date = fields.Str(required=True)
    image = fields.Str(required=True)
    url = fields.Str(required=True)
    countries = fields.Nested(
        "countriesSchema",
        only=(
            "id",
            "type",
            "name",
        ),
        required=True,
        many=True,
    )
    themes = fields.Nested(
        "themesSchema",
        only=(
            "id",
            "type",
            "name",
        ),
        required=True,
        many=True,
    )
    news = fields.Nested(
        "newsSchema",
        only=(
            "id",
            "title",
            "type",
        ),
        required=True,
        many=True,
    )


class countriesSchema(ma.Schema):
    charities = fields.Nested(
        "charitiesSchema",
        only=(
            "id",
            "type",
            "name",
        ),
        required=True,
        many=True,
    )
    news = fields.Nested(
        "newsSchema",
        only=(
            "id",
            "type",
            "title",
        ),
        required=True,
        many=True,
    )
    id = fields.Str(required=True)
    type = fields.Str(required=True, attribute="type_name")
    name = fields.Str(required=True)
    language = fields.Str(required=True)
    region = fields.Str(required=True)
    population = fields.Str(required=True)
    currency = fields.Str(required=True)
    lat = fields.Float(required=True)
    long = fields.Float(required=True)
    dwellings_no_facilities_rate = fields.Float(required=True)
    household_net_worth = fields.Float(required=True)
    employment_rate = fields.Float(required=True)
    education_rate = fields.Float(required=True)
    life_expectancy = fields.Float(required=True)
    life_satisfaction_rating = fields.Float(required=True)
    homicide_rate = fields.Float(required=True)
    long_hours_rate = fields.Float(required=True)
    voter_turnout = fields.Float(required=True)
    flag_img = fields.Str(required=True)


countries_schema = countriesSchema()
all_countries_schema = countriesSchema(many=True)
charities_schema = charitiesSchema()
all_charities_schema = charitiesSchema(many=True)
news_schema = newsSchema()
all_news_schema = newsSchema(many=True)
themes_schema = themesSchema()
all_themes_schema = themesSchema(many=True)
