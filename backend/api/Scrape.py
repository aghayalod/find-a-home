# flask imports
from flask import Flask
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import Query
import requests
import csv

#import models
from Models import Countries, Charities, Theme, News


from Init import db

from xml.etree import ElementTree

import urllib.request

from slugify import slugify

countries: dict
charities: dict
news: dict

# mapping from iso2 to iso3 codes, need to link models to countries
codes: dict = dict()


def main():
    # create tables
    db.create_all()

    # scrape_countries()
    # scrape_charities()
    # scrape_news()
    # fix_news_charity_links()

    db.session.commit()

def fix_news_charity_links():
    pass
    # # Iterate through every charity
    # for charity in Charities.query.all():
    #     # Search for news articles with the words in the charity title
    #     temp: list = charity.id.split("-")
    #     keywords: list = list()
    #     word: str
    #     add = False
    #     for word in temp:
    #         add = not add
    #         if not word.isdigit() and len(word) > 3 and add:
    #             keywords.append(word)
    #     filter = [db.or_(News.title.ilike("%"+keyword+"%"), News.description.ilike("%"+keyword+"%")) for keyword in keywords]
    #     results = News.query.filter(*filter).all()
    #     if len(results) >= 0:
    #         print("Charity name: ", charity.name)
    #         print("Results: ", len(results))
    #         print(*results, sep="\n")

def scrape_charities():
    global charities
    print("Scraping charities")
    charities = {}
    themes: dict = dict()
    # Probably a more secure way to store this but whatever
    key = "ebb95ddf-42c5-4182-9faa-753882c9c93c"
    params = {"api_key": key}
    request = requests.get("https://api.globalgiving.org/api/public/projectservice/all/projects/active/download.xml",
                           params=params)
    if request:
        # Get url from xml response
        xml = ElementTree.fromstring(request.content)
        url = xml[0].text
        # Download all charity projects
        with urllib.request.urlopen(url) as file:
            data = ElementTree.fromstring(file.read().decode('utf-8'))
            for project in data.findall("project"):
                name = project.find("image").find("title").text
                print(" Scraping", name)
                #create model with id
                charity: Charities = Charities(slugify(name))
                #assign name
                charity.name = name
                #assign other values using tags
                charity.activities = project.find("activities").text
                #construct address using various values
                address = str(project.find("contactAddress").text) + ' , ' + str(project.find("contactCity").text) + ' , ' + str(project.find("contactCountry").text)
                charity.address = address
                charity.approval_date = project.find("approvedDate").text[0:10]
                print(" Getting countries for charity")
                temp = []
                for entry in project.find("countries").findall("country"):
                    country: Countries = countries.get(codes[entry.find("iso3166CountryCode").text])
                    print("  ", country.name)
                    temp.append(country)
                [charity.countries.append(x) for x in temp if x not in charity.countries]
                print(" Getting themes for charity")
                temp = []
                for entry in project.find("themes").findall("theme"):
                    id = entry.find("id").text
                    theme = themes.get(id, Theme(id, entry.find("name").text))
                    print("  ", theme.name)
                    themes[theme.id] = theme
                    temp.append(theme)
                [charity.themes.append(x) for x in temp if x not in charity.themes]
                charity.funding = float(project.find("funding").text)
                charity.goal = float(project.find("goal").text)
                charity.status = project.find("status").text
                charity.url = project.find("projectLink").text
                charity.image = project.find("imageLink").text
                charity.total_donations = int(project.find("numberOfDonations").text)
                charity.organization = project.find("organization").find("name").text
                charities[charity.id] = charity
    for theme in themes:
        db.session.merge(themes[theme])
    for charity in charities:
        db.session.merge(charities[charity])


def scrape_countries():
    global countries
    print("Scraping countries")
    countries = dict()

    request = requests.get("https://restcountries.com/v3.1/all")
    if request:
        for entry in request.json():
            # cca3 is the same as iso3
            country: Countries = Countries(entry["cca3"], entry["name"]["common"])
            country.ico2 = entry["cca2"]
            codes[entry["cca2"]] = entry["cca3"]
            # Get only 1st currency? Not sure if we care about having a list of all currencies
            if "currencies" in entry:
                country.currency = next(iter(entry["currencies"]))
            country.region = entry["region"]
            # Get only 1st language? NOt sure if we care about having a list of all languages
            # May replace with a more robust solution later
            if "languages" in entry:
                country.language = entry["languages"][next(iter(entry["languages"]))]
            country.lat = entry["latlng"][0]
            country.long = entry["latlng"][1]
            country.flag_img = entry["flags"]["svg"]
            country.population = entry["population"]
            countries[country.id] = country

    # Scrape Better Life Index (BLI)
    request = requests.get(
        "http://stats.oecd.org/SDMX-JSON/data/BLI/.HO_BASE+IW_HNFW+JE_EMPL+ES_EDUA+CG_VOTO+HS_LEB+SW_LIFS+PS_REPH+WL_EWLH.L.TOT/all")
    if request:
        result = request.json()
        structure = result["structure"]["dimensions"]["observation"]
        # Index into the location list to get the country of the data point
        location: list = structure[0]["values"]
        for i in range(0, len(location)):
            location[i] = location[i]["id"]

        # Used to assign model values
        numValues = 9

        def assign_value(country: Countries, index, value):
            match index:
                case 0:
                    country.dwellings_no_facilities_rate = value
                    return
                case 1:
                    country.household_net_worth = value
                    return
                case 2:
                    country.employment_rate = value
                    return
                case 3:
                    country.education_rate = value
                    return
                case 4:
                    country.voter_turnout = value
                    return
                case 5:
                    country.life_expectancy = value
                    return
                case 6:
                    country.life_satisfaction_rating = value
                    return
                case 7:
                    country.homicide_rate = value
                    return
                case 8:
                    country.long_hours_rate = value
                    return

        # takes keys in the form of 'locationIndex:valueIndex:0:0' ex: '4:1:0:0'
        data: dict = result["dataSets"][0]["observations"]

        for i in range(0, len(location)):
            country: Countries = countries.get(location[i])
            if country is None:
                continue
            for j in range(0, numValues):
                key = str(i) + ":" + str(j) + ":0:0"
                value = data.get(key)
                if value is not None:
                    assign_value(country, j, value[0])

            countries[country.id] = country

    for country in countries:
        db.session.merge(countries[country])


def initialize_codes():
    global codes
    for country in Countries.query.all():
        codes[country.id] = country.ico2
        codes[country.ico2] = country.id
    print(codes)

def scrape_news():
    print("Scraping news")
    organizations: dict = dict()

    # Iterate through every charity
    for charity in Charities.query.all():
        if charity.organization not in organizations:
            organizations[charity.organization] = [charity]
        else:
            organizations[charity.organization].append(charity)
    print(organizations)

    initialize_codes()

    # Probably a more secure way to store this but whatever
    key = "fbebca055d64f5a2b78b2e76a63439e1"
    params = {"access_key": key, "limit": 100}

    print(len(organizations))

    news = {}
    for story in News.query.all():
        news[story.id] = story

    organization: str
    modified_news = []
    for organization in organizations.keys():
        if organization is None:
            continue
        modified_news.clear()
        params["keywords"] = slugify(organization).replace("-", " ")
        print("Requesting: ", organization)
        request = requests.get("http://api.mediastack.com/v1/news", params=params)
        print("URL: ", request.url, " status: ", request.status_code)
        print(request.headers)
        if request:
            result = request.json()
            for entry in result["data"]:
                modified: bool = False
                # standardize title for id
                id = slugify(entry["title"])
                story = news.get(id)
                if story is None:
                    modified = True
                    # Make new story
                    story = News(id)
                    story.country_id = codes.get(entry["country"].upper())
                    story.title = entry["title"]
                    #if no authour just use publisher
                    author = entry["author"]
                    if author == None:
                        author = entry["source"]
                    story.author = author
                    story.category = entry["category"]
                    story.description = entry["description"]
                    story.language=entry["language"]
                    story.published_at=entry["published_at"][0:10]
                    story.source=entry["source"]
                    story.url=entry["url"]
                    story.image=entry["image"]

                for charity in organizations[organization]:
                    if charity not in story.charities:
                        story.charities.append(charity)
                        modified = True
                news[story.id] = story
                if modified:
                    modified_news.append(story)
        else:
            print(request.content)
            print("Retry-After: ", request.headers.get["Retry-After"])
            break
        # merge data into db
        for story in modified_news:
            try:
                db.session.merge(story)
                db.session.commit()
            finally:
                pass











if __name__ == "__main__":
    main()
