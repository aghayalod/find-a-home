from unittest import main, TestCase
from wsgiref import headers
import requests
import json


class BackendTests(TestCase):
    # test to see if can access basic route
    def test_0(self):
        result = requests.get("https://api.findahome.me/")
        assert result.status_code == 200

    # test get all countries
    def test_1(self):
        result = requests.get("https://api.findahome.me/countries")
        assert result.status_code == 200
        jsonRes = result.json()
        assert len(jsonRes["result"]) == 10

    def test_1_2(self):
        result = requests.get("https://api.findahome.me/countries/AUS")
        jsonRes = result.json()
        assert int(jsonRes['population']) == 25687041
        

    # test get all charities
    def test_2(self):
        result = requests.get("https://api.findahome.me/charities")
        assert result.status_code == 200
        jsonRes = result.json()
        assert len(jsonRes["result"]) == 10

    def test_2_1(self):
        result = requests.get("http://api.findahome.me/charities/1-for-higher-education-project")
        jsonRes = result.json()
        assert float(jsonRes['funding']) == 18860.1

    # test get all news
    def test_3(self):
        result = requests.get("http://api.findahome.me/news")
        assert result.status_code == 200
        jsonRes = result.json()
        assert len(jsonRes["result"]) == 10

    def test_3_1(self):
        result = requests.get("http://api.findahome.me/news/0-04-earnings-per-share-expected-for-casa-systems-inc-nasdaq-casa-this-quarter")
        jsonRes = result.json()
        assert jsonRes['source'] == 'etfdailynews'


if __name__ == "__main__":
    main()
