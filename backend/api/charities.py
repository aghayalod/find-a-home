from sqlalchemy import and_, or_, func
from sqlalchemy.sql.expression import all_, cast
from Models import Charities, Theme
import datetime


def search_charities(query, parameters):
    if "search" in parameters:
        for parameter in parameters["search"]:
            keywords = parameter.split(" ")
            filters = []
            for keyword in keywords:
                filters.append(
                    or_(
                        Charities.name.contains(keyword),
                        Charities.address.contains(keyword),
                        Charities.activities.contains(keyword),
                        Charities.status.contains(keyword),
                        Charities.organization.contains(keyword),
                    )
                )
            query = query.filter(or_(*filters))
    return query


def filter_charities(query, parameters):

    if "name" in parameters:
        charities_filter = parameters["name"]
        print(charities_filter)
        query = query.filter(Charities.name.in_(charities_filter))

    if "address" in parameters:
        address_filter = parameters["address"]
        query = query.filter(Charities.address.in_(address_filter))

    if "activities" in parameters:
        activities_filter = parameters["activities"]
        query = query.filter(Charities.activities.in_(activities_filter))

    if "themes" in parameters:
        themes_filter = parameters["themes"]
        print(themes_filter)
        query = query.filter(Charities.themes.any(Theme.id.in_(themes_filter)))

    if "status" in parameters:
        status_filter = parameters["status"]
        query = query.filter(Charities.status.in_(status_filter))

    if "total_donations" in parameters:
        total_donations_filter = parameters["total_donations"]
        total_filters = []
        for filter in total_donations_filter:
            if ">" in filter:
                lower_bound = filter.split(">")[1]
                total_filters.append(
                    and_(
                        Charities.total_donations > int(lower_bound),
                    )
                )
            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                total_filters.append(
                    and_(
                        Charities.total_donations < int(upper_bound),
                    )
                )
            else:
                lower_bound, upper_bound = filter.split("-")
                total_filters.append(
                    and_(
                        Charities.total_donations > int(lower_bound),
                        Charities.total_donations <= int(upper_bound),
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    if "approval_date" in parameters:
        date_filter = parameters["approval_date"]
        total_filters = []
        for filter in date_filter:
            if ">" in filter:
                lower_bound = filter.split(">")[1]
                lower_bound = datetime.datetime.strptime(lower_bound, "%Y/%m/%d")
                total_filters.append(and_(Charities.approval_date > lower_bound))

            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                upper_bound = datetime.datetime.strptime(upper_bound, "%Y/%m/%d")
                total_filters.append(and_(Charities.approval_date < upper_bound))
            else:
                lower_bound, upper_bound = filter.split("-")
                upper_bound = datetime.datetime.strptime(upper_bound, "%Y/%m/%d")
                lower_bound = datetime.datetime.strptime(lower_bound, "%Y/%m/%d")
                total_filters.append(
                    and_(
                        Charities.approval_date > lower_bound,
                        Charities.approval_date <= upper_bound,
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    if "goal" in parameters:
        goal_filter = parameters["goal"]
        total_filters = []
        for filter in goal_filter:
            if ">" in filter:
                lower_bound = filter.split(">")[1]
                total_filters.append(
                    and_(
                        Charities.goal > float(lower_bound),
                    )
                )
            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                total_filters.append(
                    and_(
                        Charities.goal < float(upper_bound),
                    )
                )
            else:
                lower_bound, upper_bound = filter.split("-")
                total_filters.append(
                    and_(
                        Charities.goal > float(lower_bound),
                        Charities.goal <= float(upper_bound),
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    if "funding" in parameters:
        funding_filter = parameters["funding"]
        total_filters = []
        for filter in funding_filter:
            if ">" in filter:
                lower_bound = filter.split(">")[1]
                total_filters.append(
                    and_(
                        Charities.funding > float(lower_bound),
                    )
                )
            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                total_filters.append(
                    and_(
                        Charities.funding < float(upper_bound),
                    )
                )
            else:
                lower_bound, upper_bound = filter.split("-")
                total_filters.append(
                    and_(
                        Charities.funding > float(lower_bound),
                        Charities.funding <= float(upper_bound),
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    return query


def sort_charities(query, parameters):
    if "sort" in parameters:
        param_attr, order = parameters["sort"][0].split("-")

        curr_attr = None
        if param_attr == "name":
            curr_attr = Charities.name
        elif param_attr == "address":
            curr_attr = Charities.address
        elif param_attr == "activities":
            curr_attr = Charities.activities
        elif param_attr == "status":
            curr_attr = Charities.status
        elif param_attr == "total_donations":
            curr_attr = Charities.total_donations
        elif param_attr == "funding":
            curr_attr = Charities.funding
        elif param_attr == "goal":
            curr_attr = Charities.goal
        elif param_attr == "approval_date":
            curr_attr = Charities.approval_date
        elif param_attr == "image":
            curr_attr = Charities.image
        elif param_attr == "url":
            curr_attr = Charities.url
        elif param_attr == "organization":
            curr_attr = Charities.organization

        if curr_attr:
            return query.order_by(curr_attr.desc() if order == "dec" else curr_attr)
    return query
