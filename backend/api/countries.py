from sqlalchemy import and_, or_, func
from sqlalchemy.sql.expression import all_, cast
from Models import Countries


def search_countries(query, parameters):
    if "search" in parameters:
        for parameter in parameters["search"]:
            keywords = parameter.split(" ")
            filters = []
            for keyword in keywords:
                filters.append(
                    or_(
                        Countries.name.contains(keyword),
                        Countries.currency.contains(keyword),
                        Countries.language.contains(keyword),
                        Countries.region.contains(keyword),
                    )
                )
            query = query.filter(or_(*filters))
    return query


def filter_countries(query, parameters):

    if "name" in parameters:
        countries_filter = parameters["name"]
        print(countries_filter)
        query = query.filter(Countries.name.in_(countries_filter))

    if "language" in parameters:
        language_filter = parameters["language"]
        query = query.filter(Countries.language.in_(language_filter))

    if "currency" in parameters:
        currency_filter = parameters["currency"]
        query = query.filter(Countries.currency.in_(currency_filter))

    if "region" in parameters:
        region_filter = parameters["region"]
        query = query.filter(Countries.region.in_(region_filter))

    if "population" in parameters:
        population_filter = parameters["population"]
        total_filters = []
        for filter in population_filter:

            if ">" in filter:
                lower_bound = filter.split(">")[1]
                total_filters.append(
                    and_(
                        Countries.population > int(lower_bound),
                    )
                )
            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                total_filters.append(
                    and_(
                        Countries.population < int(upper_bound),
                    )
                )
            else:
                lower_bound, upper_bound = filter.split("-")
                total_filters.append(
                    and_(
                        Countries.population > int(lower_bound),
                        Countries.population <= int(upper_bound),
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    if "life_expectancy" in parameters:
        life_filter = parameters["life_expectancy"]
        total_filters = []
        for filter in life_filter:
            if ">" in filter:
                lower_bound = filter.split(">")[1]
                total_filters.append(
                    and_(
                        Countries.life_expectancy > float(lower_bound),
                    )
                )
            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                total_filters.append(
                    and_(
                        Countries.life_expectancy < float(upper_bound),
                    )
                )
            else:
                lower_bound, upper_bound = filter.split("-")
                total_filters.append(
                    and_(
                        Countries.life_expectancy > float(lower_bound),
                        Countries.life_expectancy <= float(upper_bound),
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    if "education_rate" in parameters:
        education_filter = parameters["education_rate"]
        total_filters = []
        for filter in education_filter:
            if ">" in filter:
                lower_bound = filter.split(">")[1]
                total_filters.append(
                    and_(
                        Countries.education_rate > float(lower_bound),
                    )
                )
            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                total_filters.append(
                    and_(
                        Countries.education_rate < float(upper_bound),
                    )
                )
            else:
                lower_bound, upper_bound = filter.split("-")
                total_filters.append(
                    and_(
                        Countries.education_rate > float(lower_bound),
                        Countries.education_rate <= float(upper_bound),
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    if "homicide_rate" in parameters:
        homicide_filter = parameters["homicide_rate"]
        total_filters = []
        for filter in homicide_filter:
            if ">" in filter:
                lower_bound = filter.split(">")[1]
                total_filters.append(
                    and_(
                        Countries.homicide_rate > float(lower_bound),
                    )
                )
            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                total_filters.append(
                    and_(
                        Countries.homicide_rate < float(upper_bound),
                    )
                )
            else:
                lower_bound, upper_bound = filter.split("-")
                total_filters.append(
                    and_(
                        Countries.homicide_rate > float(lower_bound),
                        Countries.homicide_rate <= float(upper_bound),
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    if "household_net_worth" in parameters:
        household_net_worth_filter = parameters["household_net_worth"]
        total_filters = []
        for filter in household_net_worth_filter:
            if ">" in filter:
                lower_bound = filter.split(">")[1]
                total_filters.append(
                    and_(
                        Countries.household_net_worth > float(lower_bound),
                    )
                )
            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                total_filters.append(
                    and_(
                        Countries.household_net_worth < float(upper_bound),
                    )
                )
            else:
                lower_bound, upper_bound = filter.split("-")
                total_filters.append(
                    and_(
                        Countries.household_net_worth > float(lower_bound),
                        Countries.household_net_worth <= float(upper_bound),
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    if "life_satisfaction_rating" in parameters:
        life_satisfaction_rating_filter = parameters["life_satisfaction_rating"]
        total_filters = []
        for filter in life_satisfaction_rating_filter:
            if ">" in filter:
                lower_bound = filter.split(">")[1]
                total_filters.append(
                    and_(
                        Countries.life_satisfaction_rating > float(lower_bound),
                    )
                )
            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                total_filters.append(
                    and_(
                        Countries.life_satisfaction_rating < float(upper_bound),
                    )
                )
            else:
                lower_bound, upper_bound = filter.split("-")
                total_filters.append(
                    and_(
                        Countries.life_satisfaction_rating > float(lower_bound),
                        Countries.life_satisfaction_rating <= float(upper_bound),
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    if "employment_rate" in parameters:
        employment_rate_filter = parameters["employment_rate"]
        total_filters = []
        for filter in employment_rate_filter:
            if ">" in filter:
                lower_bound = filter.split(">")[1]
                total_filters.append(
                    and_(
                        Countries.employment_rate > float(lower_bound),
                    )
                )
            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                total_filters.append(
                    and_(
                        Countries.employment_rate < float(upper_bound),
                    )
                )
            else:
                lower_bound, upper_bound = filter.split("-")
                total_filters.append(
                    and_(
                        Countries.employment_rate > float(lower_bound),
                        Countries.employment_rate <= float(upper_bound),
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    if "dwellings_no_facilities_rate" in parameters:
        dwellings_no_facilities_rate_filter = parameters["dwellings_no_facilities_rate"]
        total_filters = []
        for filter in dwellings_no_facilities_rate_filter:
            if ">" in filter:
                lower_bound = filter.split(">")[1]
                total_filters.append(
                    and_(
                        Countries.dwellings_no_facilities_rate > float(lower_bound),
                    )
                )
            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                total_filters.append(
                    and_(
                        Countries.dwellings_no_facilities_rate < float(upper_bound),
                    )
                )
            else:
                lower_bound, upper_bound = filter.split("-")
                total_filters.append(
                    and_(
                        Countries.dwellings_no_facilities_rate > float(lower_bound),
                        Countries.dwellings_no_facilities_rate <= float(upper_bound),
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    if "lat" in parameters:
        lat_filter = parameters["lat"]
        total_filters = []
        for filter in lat_filter:
            if ">" in filter:
                lower_bound = filter.split(">")[1]
                total_filters.append(
                    and_(
                        Countries.lat > float(lower_bound),
                    )
                )
            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                total_filters.append(
                    and_(
                        Countries.lat < float(upper_bound),
                    )
                )
            else:
                lower_bound, upper_bound = filter.split("-")
                total_filters.append(
                    and_(
                        Countries.lat > float(lower_bound),
                        Countries.lat <= float(upper_bound),
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    if "long" in parameters:
        long_filter = parameters["long"]
        total_filters = []
        for filter in long_filter:
            if ">" in filter:
                lower_bound = filter.split(">")[1]
                total_filters.append(
                    and_(
                        Countries.long > float(lower_bound),
                    )
                )
            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                total_filters.append(
                    and_(
                        Countries.long < float(upper_bound),
                    )
                )
            else:
                lower_bound, upper_bound = filter.split("-")
                total_filters.append(
                    and_(
                        Countries.long > float(lower_bound),
                        Countries.long <= float(upper_bound),
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    if "long_hours_rate" in parameters:
        long_hours_rate_filter = parameters["long_hours_rate"]
        total_filters = []
        for filter in long_hours_rate_filter:
            if ">" in filter:
                lower_bound = filter.split(">")[1]
                total_filters.append(
                    and_(
                        Countries.long_hours_rate > float(lower_bound),
                    )
                )
            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                total_filters.append(
                    and_(
                        Countries.long_hours_rate < float(upper_bound),
                    )
                )
            else:
                lower_bound, upper_bound = filter.split("-")
                total_filters.append(
                    and_(
                        Countries.long_hours_rate > float(lower_bound),
                        Countries.long_hours_rate <= float(upper_bound),
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    if "voter_turnout" in parameters:
        voter_turnout_filter = parameters["voter_turnout"]
        total_filters = []
        for filter in voter_turnout_filter:
            if ">" in filter:
                lower_bound = filter.split(">")[1]
                total_filters.append(
                    and_(
                        Countries.voter_turnout > float(lower_bound),
                    )
                )
            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                total_filters.append(
                    and_(
                        Countries.voter_turnout < float(upper_bound),
                    )
                )
            else:
                lower_bound, upper_bound = filter.split("-")
                total_filters.append(
                    and_(
                        Countries.voter_turnout > float(lower_bound),
                        Countries.voter_turnout <= float(upper_bound),
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    return query


def sort_countries(query, parameters):
    if "sort" in parameters:
        param_attr, order = parameters["sort"][0].split("-")

        curr_attr = None
        if param_attr == "name":
            curr_attr = Countries.name
        elif param_attr == "population":
            curr_attr = Countries.population
        elif param_attr == "education_rate":
            curr_attr = Countries.education_rate
        elif param_attr == "language":
            curr_attr = Countries.language
        elif param_attr == "region":
            curr_attr = Countries.region
        elif param_attr == "currency":
            curr_attr = Countries.currency
        elif param_attr == "lat":
            curr_attr = Countries.lat
        elif param_attr == "long":
            curr_attr = Countries.long
        elif param_attr == "dwellings_no_facilities_rate":
            curr_attr = Countries.dwellings_no_facilities_rate
        elif param_attr == "household_net_worth":
            curr_attr = Countries.household_net_worth
        elif param_attr == "employment_rate":
            curr_attr = Countries.employment_rate
        elif param_attr == "life_expectancy":
            curr_attr = Countries.life_expectancy
        elif param_attr == "life_satisfaction_rating":
            curr_attr = Countries.life_satisfaction_rating
        elif param_attr == "homicide_rate":
            curr_attr = Countries.homicide_rate
        elif param_attr == "long_hours_rate":
            curr_attr = Countries.long_hours_rate
        elif param_attr == "voter_turnout":
            curr_attr = Countries.voter_turnout

        if curr_attr:
            return query.order_by(curr_attr.desc() if order == "dec" else curr_attr)
    return query
