from time import strptime
from sqlalchemy import and_, or_, func
from sqlalchemy.sql.expression import all_, cast
from Models import News
import datetime


def search_news(query, parameters):
    if "search" in parameters:
        for parameter in parameters["search"]:
            keywords = parameter.split(" ")
            filters = []
            for keyword in keywords:
                filters.append(
                    or_(
                        News.title.contains(keyword),
                        News.source.contains(keyword),
                        News.description.contains(keyword),
                        News.author.contains(keyword),
                    )
                )
            query = query.filter(or_(*filters))
    return query


def filter_news(query, parameters):

    if "source" in parameters:
        source_filter = parameters["source"]
        print(source_filter)
        query = query.filter(News.source.in_(source_filter))

    if "language" in parameters:
        language_filter = parameters["language"]
        query = query.filter(News.language.in_(language_filter))

    if "category" in parameters:
        category_filter = parameters["category"]
        query = query.filter(News.category.in_(category_filter))

    if "author" in parameters:
        author_filter = parameters["author"]
        query = query.filter(News.author.in_(author_filter))

    if "published_at" in parameters:
        date_filter = parameters["published_at"]
        total_filters = []
        for filter in date_filter:
            if ">" in filter:
                lower_bound = filter.split(">")[1]
                lower_bound = datetime.datetime.strptime(lower_bound, "%Y/%m/%d")
                total_filters.append(and_(News.published_at > lower_bound))

            elif "<" in filter:
                upper_bound = filter.split("<")[1]
                upper_bound = datetime.datetime.strptime(upper_bound, "%Y/%m/%d")
                total_filters.append(and_(News.published_at < upper_bound))
            else:
                lower_bound, upper_bound = filter.split("-")
                upper_bound = datetime.datetime.strptime(upper_bound, "%Y/%m/%d")
                lower_bound = datetime.datetime.strptime(lower_bound, "%Y/%m/%d")
                total_filters.append(
                    and_(
                        News.published_at > lower_bound,
                        News.published_at <= upper_bound,
                    )
                )
        query = query.filter(or_(*tuple(total_filters)))

    if "country_id" in parameters:
        country_id_filter = parameters["country_id"]
        query = query.filter(News.country_id.in_(country_id_filter))

    return query


def sort_news(query, parameters):
    if "sort" in parameters:
        param_attr, order = parameters["sort"][0].split("-")

        curr_attr = None
        if param_attr == "source":
            curr_attr = News.source
        elif param_attr == "title":
            curr_attr = News.title
        elif param_attr == "description":
            curr_attr = News.description
        elif param_attr == "url":
            curr_attr = News.url
        elif param_attr == "author":
            curr_attr = News.author
        elif param_attr == "country_id":
            curr_attr = News.country_id
        elif param_attr == "published_at":
            curr_attr = News.published_at
        elif param_attr == "language":
            curr_attr = News.language
        elif param_attr == "category":
            curr_attr = News.category
        elif param_attr == "image":
            curr_attr = News.image

        if curr_attr:
            return query.order_by(curr_attr.desc() if order == "dec" else curr_attr)
    return query
