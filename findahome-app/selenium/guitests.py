from unittest import main, TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
import time
import sys

PATH = "chromedriver_linux"
class UITests(TestCase):
    HOME = "https://develop.dbys0yebl1mry.amplifyapp.com"
    COUNTRY_LINK = HOME + "/country"
    CHARITY_LINK = HOME + "/charity"
    NEWS_LINK = HOME + "/news"
    ABOUT_LINK = HOME + "/about"

    # enable driver
    @classmethod
    def setUpClass(self):
        chrome_options = Options()
        chrome_options.add_argument("--enable-javascript")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_prefs = {}
        chrome_options.experimental_options["prefs"] = chrome_prefs
        # Disable images
        chrome_prefs["profile.default_content_settings"] = {"images": 2}
        self.driver = webdriver.Chrome(options=chrome_options)

    # close driver
    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def testHome(self):
        self.driver.get(self.HOME)
        result = self.driver.find_element(By.CSS_SELECTOR, "#root")
        self.assertNotEqual(result, None)

    # retrieve source code
    def testHome_2(self):
        self.driver.get(self.HOME)
        result = self.driver.page_source
        time.sleep(2)
        self.assertNotEqual(result, None)

    # retrieve about page
    def testAbout(self):
        self.driver.get(self.ABOUT_LINK)
        result = self.driver.find_element(By.CSS_SELECTOR, "#root")
        self.assertNotEqual(result, None)

    # retrieve source code
    def testAbout_2(self):
        self.driver.get(self.ABOUT_LINK)
        result = self.driver.page_source
        time.sleep(2)
        self.assertNotEqual(result, None)

    

    # retrieve country page
    def testCountry(self):
        self.driver.get(self.COUNTRY_LINK)
        result = self.driver.find_element(By.CSS_SELECTOR, "#root")
        self.assertNotEqual(result, None)

    # retrieve source code
    def testCountry_2(self):
        self.driver.get(self.COUNTRY_LINK)
        result = self.driver.page_source
        time.sleep(2)
        self.assertNotEqual(result, None)

    # retrieve charities page
    def testCharities(self):
        self.driver.get(self.CHARITY_LINK)
        result = self.driver.find_element(By.CSS_SELECTOR, "#root")
        self.assertNotEqual(result, None)

    # retrieve source code
    def testCharity_2(self):
        self.driver.get(self.CHARITY_LINK)
        result = self.driver.page_source
        time.sleep(2)
        self.assertNotEqual(result, None)

    # retrieve news page
    def testNews(self):
        self.driver.get(self.NEWS_LINK)
        result = self.driver.find_element(By.CSS_SELECTOR, "#root")
        self.assertNotEqual(result, None)
    
    # retrieve source code
    def testNews_2(self):
        self.driver.get(self.NEWS_LINK)
        result = self.driver.page_source
        time.sleep(2)
        self.assertNotEqual(result, None)
    
    #test searching
    def testSearch(self):
        self.driver.get(self.HOME)
        result = self.driver.find_elements(By.CSS_SELECTOR, ".search_container")
        self.assertNotEqual(result, None)

    def testSearcNews(self):
        self.driver.get(self.NEWS_LINK)
        result = self.driver.find_elements(By.CSS_SELECTOR, ".Home_search_container__xeCae")
        self.assertNotEqual(result, None)

    def testSearcCountries(self):
        self.driver.get(self.COUNTRY_LINK)
        result = self.driver.find_elements(By.CSS_SELECTOR, ".Home_search_container__xeCae")
        self.assertNotEqual(result, None)

    def testSearcCharities(self):
        self.driver.get(self.CHARITY_LINK)
        result = self.driver.find_elements(By.CSS_SELECTOR, ".Home_search_container__xeCae")
        self.assertNotEqual(result, None)
    #test 5 attributes for filtering and sorting

    def testFilterSortNews(self):
        self.driver.get(self.NEWS_LINK)
        result = self.driver.find_elements(By.CSS_SELECTOR, "#dropdown-basic-button")
        print(len(result))
        self.assertEqual(len(result), 6)
    
    def testFilterSortCountry(self):
        self.driver.get(self.COUNTRY_LINK)
        result = self.driver.find_elements(By.CSS_SELECTOR, "#dropdown-basic-button")
        print(len(result))
        self.assertEqual(len(result), 6)
    
    def testFilterSortCharity(self):
        self.driver.get(self.CHARITY_LINK)
        result = self.driver.find_elements(By.CSS_SELECTOR, "#dropdown-basic-button")
        print(len(result))
        self.assertEqual(len(result), 6)
    
    

if __name__ == '__main__':
    # PATH = sys.argv[1]
    main()
