import './App.css';
import React, { useState, useEffect } from "react";
import { Button, Row, Col } from "react-bootstrap";
import Searchbar from "./search/Searchbar.js"
import Charity from "./pages/models/Charity"
import Country from "./pages/models/Country"
import News from "./pages/models/News"
import About from "./pages/about/About"
import ProviderVisualizations from "./pages/provider-visualizations/ProviderVisualizations"
import OurVisualizations from "./pages/our-visualizations/OurVisualizations"
import "bootstrap/dist/css/bootstrap.min.css"
import {
  Navbar,
  Nav,
  Container
} from 'react-bootstrap'

import styles from "./Home.module.css"
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  useNavigate,
  createSearchParams
} from "react-router-dom";

import CharityRouter from './pages/charity/CharityRouter';
import CountryRouter from './pages/countries/CountryRouter';
import NewsRouter from './pages/news/NewsRouter';
import { SearchPage } from './search/SearchPage';
import logo from './components/logo.png'

function Navigation() {
  return (
    <Navbar className={styles.color_nav} variant="dark" >
      <Container fluid>
        <Navbar.Brand as={Link} to="/">
          <img
            alt=""
            src={logo}
            width="30"
            height="30"
            className="d-inline-block align-top"
          />{' '}
        Find A Home</Navbar.Brand>
        <Nav>
          <Nav.Link as={Link} to="/">Home</Nav.Link>
          <Nav.Link as={Link} to="/about">About</Nav.Link>
          <Nav.Link as={Link} to="/country">Countries</Nav.Link>
          <Nav.Link as={Link} to="/news">News</Nav.Link>
          <Nav.Link as={Link} to="/charity">Charities</Nav.Link>
          <Nav.Link as={Link} to="/provider-visualizations">Provider Visualizations</Nav.Link>
          <Nav.Link as={Link} to="/our-visualizations">Our Visualizations</Nav.Link>
          <Nav.Link as={Link} to="/search">Search</Nav.Link>
       </Nav>
      </Container>
    </Navbar>
  )
}

export function useFetch(url) {
  const [{ items, isLoading }, setItems] = useState({ items: [], isLoading: true });
  useEffect(() => {
    console.log("Fetching data from " + url);
    setItems({ items: [], isLoading: true });
    (async function () {
      const response = await fetch(url, {
        method: 'GET',
        mode: 'cors',
        headers: {
          'accept': 'application/vnd.api+json',
        },
      });
      const result = await response.json();
      setItems({ items: result, isLoading: false });
    })()
  }, [url]);
  return [items, isLoading];
}

function App() {

  // const [countryData, loading] = useFetch(`https://api.findahome.me/api/countries?page[size]=10&page[number]=1`);
  // const [newsData, loading2] = useFetch(`https://api.findahome.me/api/news?page[size]=10&page[number]=1`);
  // const [charityData, loading3] = useFetch(`https://api.findahome.me/api/charities?page[size]=10&page[number]=1`);
  // const NUM_COUNTRIES = countryData.meta.total;
  // const NUM_NEWS = newsData.meta.total;
  // const NUM_CHARITIES = charityData.meta.total;

  return (
    <Router>
      <div>
        <Navigation />
        <Routes>
          <Route exact path='/' element={<Home />} />
          <Route path={`/about/*`} element={<About />} />

          <Route path={`/country/*`} element={<Country />} />
          <Route path={`/news/*`} element={<News />} />
          <Route path={`/charity/*`} element={<Charity />} />
          <Route path={`/provider-visualizations/*`} element={<ProviderVisualizations />} />
          <Route path={`/our-visualizations/*`} element={<OurVisualizations />} />
          <Route path="country/:id" element={<CountryRouter />} />
          <Route path="news/:id" element={<NewsRouter />} />
          <Route path="charity/:id" element={<CharityRouter />} />
          <Route path="search" element={<SearchPage />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;

function Home(props) {
  const navigate = useNavigate();

  function onSearch(query) {
    navigate({
      pathname: "/search",
      search: `?${createSearchParams({ q: query })}`,
    })
  }

  return (
      <div className={styles.splashPage}>
        <div className={styles.splash}>
          <div className={styles.splashContent}>
              <div className={styles.headerText}>
              <Row>
              <p> </p>
              <p> </p>
              <p> </p>
            </Row> 
            <Row>
              <p> </p>
              <p> </p>
              <p> </p>
            </Row> 
                <h1 className={styles.main_title}><strong>Find A Home</strong></h1>
              </div>
            <Row>
              <p> </p>
              <p> </p>
              <p> </p>
            </Row>  
            <Row>
              <Col>
                <Button className={styles.color_button}href="/country">
                  <h3><strong>Countries</strong></h3>
                </Button>
              </Col>
              <Col>
                <Button className={styles.color_button}href="/news">
                  <h3><strong>News</strong></h3>
                </Button>
              </Col>
              <Col>
                <Button className={styles.color_button}href="/charity">
                  <h3><strong>Charities</strong></h3>
                </Button>
              </Col>  
            </Row>
            <Row>
              <p> </p>
              <p> </p>
              <p> </p>
            </Row> 
            <Row>
              <p> </p>
              <p> </p>
              <p> </p>
            </Row>  
            <Row>
              <div className={styles.wrapper}>
                <Searchbar onSearch={onSearch}/>
              </div>
            </Row>
            <Row>
              <p> </p>
              <p> </p>
              <p> </p>
            </Row>
            <Row>
              <p> </p>
              <p> </p>
              <p> </p>
            </Row>
          </div>
      </div>
    </div>
  );
}
