import './App.css';
import React, { useState, useEffect } from "react";
import { Button, Row, Col } from "react-bootstrap";
import Searchbar from "./search/Searchbar.js"


import Charity from "./pages/models/Charity"
import Country from "./pages/models/Country"
import News from "./pages/models/News"
import About from "./pages/about/About"
import "bootstrap/dist/css/bootstrap.min.css"
import {
  Navbar,
  Nav,
  Container
} from 'react-bootstrap'


import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link,
  useNavigate,
  createSearchParams
} from "react-router-dom";

import CharityRouter from './pages/charity/CharityRouter';
import CountryRouter from './pages/countries/CountryRouter';
import NewsRouter from './pages/news/NewsRouter';
import { SearchPage } from './search/SearchPage';


function Navigation() {
  return (
    <Navbar bg="light" expand="lg">
      <Container fluid>
        <Navbar.Brand as={Link} to="/">Find a Home</Navbar.Brand>
        <Nav>
          <Nav.Link as={Link} to="/">Home</Nav.Link>
          <Nav.Link as={Link} to="/about">About</Nav.Link>
          <Nav.Link as={Link} to="/country">Countries</Nav.Link>
          <Nav.Link as={Link} to="/news">News</Nav.Link>
          <Nav.Link as={Link} to="/charity">Charities</Nav.Link>
          <Nav.Link as={Link} to="/search">Search</Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  )
}

export function useFetch(url) {
  const [{ items, isLoading }, setItems] = useState({ items: [], isLoading: true });
  useEffect(() => {
    console.log("Fetching data from " + url);
    setItems({ items: [], isLoading: true });
    (async function () {
      const response = await fetch(url, {
        method: 'GET',
        mode: 'cors',
        headers: {
          'accept': 'application/vnd.api+json',
        },
      });
      const result = await response.json();
      setItems({ items: result, isLoading: false });
    })()
  }, [url]);
  return [items, isLoading];
}

function Temp() {

  // const [countryData, loading] = useFetch(`https://api.findahome.me/api/countries?page[size]=10&page[number]=1`);
  // const [newsData, loading2] = useFetch(`https://api.findahome.me/api/news?page[size]=10&page[number]=1`);
  // const [charityData, loading3] = useFetch(`https://api.findahome.me/api/charities?page[size]=10&page[number]=1`);
  // const NUM_COUNTRIES = countryData.meta.total;
  // const NUM_NEWS = newsData.meta.total;
  // const NUM_CHARITIES = charityData.meta.total;

  return (
    <Router>
      <div>
        <Navigation />
        <Routes>
          <Route exact path='/' element={<Home />} />
          <Route path={`/about/*`} element={<About />} />

          <Route path={`/country/*`} element={<Country />} />
          <Route path={`/news/*`} element={<News />} />
          <Route path={`/charity/*`} element={<Charity />} />

          <Route path="country/:id" element={<CountryRouter />} />
          <Route path="news/:id" element={<NewsRouter />} />
          <Route path="charity/:id" element={<CharityRouter />} />
          <Route path="search" element={<SearchPage />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;

function Home(props) {
  const navigate = useNavigate();

  function onSearch(query) {
    navigate({
      pathname: "/search",
      search: `?${createSearchParams({ q: query })}`,
    })
  }

  return (
    <div style={{
      background: `url(https://hbr.org/resources/images/article_assets/2019/02/Feb19_06_671337433.jpg)`,
      backgroundRepeat: 'no-repeat',
      width:'1200px',
      height:'800px'
    }}>
      <Container>
        <Row className="text-center m-5 pt-5">
          <h1>Find A Home</h1>
        </Row>
        <Row className="text-center m-5 pt-5">
          <Col>
            <Button href="/charity">
              <h5>Charities</h5>
              <p>Find out more about charities</p>
            </Button>
          </Col>
          <Col>
            <Button href="/country">
              <h5>Countries </h5>
              <p>Learn about countries</p>
            </Button>
          </Col>
          <Col>
            <Button href="/news">
              <h5>News Articles </h5>
              <p>See what's going on in the world</p>
            </Button>
          </Col>
        </Row>
        <Searchbar onSearch={onSearch}/>
      </Container>
    </div>

  );
}
