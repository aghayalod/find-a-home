import { configure, shallow } from "enzyme";
import toJson from 'enzyme-to-json';
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
configure({ adapter: new Adapter() });
import App from "../App"
import CharityInstance from "../pages/charity/CharityInstance"
import CharityRouter from "../pages/charity/CharityRouter"
import CountryInstance from "../pages/countries/CountryInstance"
import CountryRouter from "../pages/countries/CountryRouter"
import NewsInstance from "../pages/news/NewsInstance"
import NewsRouter from "../pages/news/NewsRouter"
import About from "../pages/about/About"
import Searchbar from "../search/Searchbar"
import CharityTable from "../pages/models/Charity"
import Filterable from "../components/Filterable"
import Sortable from "../components/Sortable"


// test 1 to check shallow call on app
test("render App", () => {
    const wrapper = shallow(<App />);
});

// test 2 that App snapshot matches
test("check App snapshot", () => {
    const example = shallow(<App />);
    expect(toJson(example)).toMatchSnapshot();
});

const mockCharity = {
        attributes: {
            name: "$1 for Higher Education Project",
            image: "https://www.globalgiving.org/pfil/26507/pict.jpg",
            activities: "donating",
            address: "601 SW Second Avenue, Suite 2100 , Portland , United States",
            funding: 18860.1,
            goal: 50000,
            approval_date: "2017-01-20",
            url: "https://www.globalgiving.org/projects/napefoundation/",
            status: "Active",
            total_donations: 170,
            themes: [{
                id: "ecdev",
                type: "themes"
            },
            {
                id: "edu",
                type: "themes"
            }],
            countries: [
                {
                    id: "usa",
                    type: "countries"
                }
            ],
            news: [{
                id: "breaking news",
                type: "news"
            }]
        },
    };

// test 3 for charity instance rendering properly
test("render Charity Instance", () => {
    const wrapper = shallow(<CharityInstance charity={mockCharity.attributes} />);
    expect(toJson(wrapper)).toMatchSnapshot();
});

// test 4 for checking charity instance properties
test("check Charity Instance properties", () => {
    const wrapper = shallow(<CharityInstance charity={mockCharity.attributes} />);
    const charityName = <strong>{mockCharity.attributes.name}</strong>
    expect(wrapper.contains(charityName)).toEqual(true);
    const charityActivities = <strong>Total Donations: </strong>;
    expect(wrapper.contains(charityActivities)).toEqual(true);
    const charityAddress = <strong>Address: </strong>;
    expect(wrapper.contains(charityAddress)).toEqual(true);
    const charityFunding = <strong>Current Funding: </strong>
    expect(wrapper.contains(charityFunding)).toEqual(true);
    const charityGoal = <strong>Goal: </strong>;
    expect(wrapper.contains(charityGoal)).toEqual(true);
    const charityApproval = <strong>Approval Date: </strong>;
    expect(wrapper.contains(charityApproval)).toEqual(true);
    const charityStatus = <strong>Status: </strong>
    expect(wrapper.contains(charityStatus)).toEqual(true);
    const charityThemes = <strong>Themes: </strong>;
    expect(wrapper.contains(charityThemes)).toEqual(true);
    expect(toJson(wrapper)).toMatchSnapshot();
});

const mockCountry = {

    attributes: {
        name:"Australia",
        language: "English",
        long: 90.00,
        lat: 90.00,
        region: "Oceania",
        population: 25687041,
        currency: "AUD",
        flag_img: "https://flagcdn.com/au.svg",
        dwellings_no_facilities_rate: 10,
        household_net_worth: 528768,
        employment_rate: 73,
        education_rate: 84,
        voter_turnout: 92,
        life_expectancy: 83,
        life_satisfaction_rating: 7.1,
        homicide_rate: 0.9,
        long_hours_rate: 12.5,
        charities: [
            {
                id: "Red Cross",
                type: "charities"
            }
        ],
        news: [
            {
                id: "Breaking News",
                type: "news"
            }
        ]
    },
};

// test 5 for country instance rendering properly
test("render Country Instance", () => {
    const wrapper = shallow(<CountryInstance country={mockCountry.attributes} />);
    expect(toJson(wrapper)).toMatchSnapshot();
});

// test 6 to check country properties
test("check Country Properties", () => {
    const wrapper = shallow(<CountryInstance country={mockCountry.attributes} />);
    const countryName = (<strong>Country: </strong>)
    //expect(wrapper.contains(countryName)).toEqual(true);
    const countryLanguage = <strong>Language(s): </strong>;
    expect(wrapper.contains(countryLanguage)).toEqual(true);
    const countryRegion = <strong>Region: </strong>
    expect(wrapper.contains(countryRegion)).toEqual(true);
    const countryPopulation = <strong>Population: </strong>;
    expect(wrapper.contains(countryPopulation)).toEqual(true);
    const countryCurrency = <strong>Currency: </strong>;
    expect(wrapper.contains(countryCurrency)).toEqual(true);
    const countryDwellings = <strong>Dwellings without basic facilities: </strong>;
    expect(wrapper.contains(countryDwellings)).toEqual(true);
    const countryHousehold = <strong>Household Net Wealth: </strong>;
    expect(wrapper.contains(countryHousehold)).toEqual(true);
    const countryEmployment = <strong>Employment Rate: </strong>;
    expect(wrapper.contains(countryEmployment)).toEqual(true);
    const countryEdu = <strong>Educational Attainment: </strong>;
    expect(wrapper.contains(countryEdu)).toEqual(true);
    const countryVote = <strong>Voter Turnout: </strong>;
    expect(wrapper.contains(countryVote)).toEqual(true);
    const countryLife = <strong>Life Expectancy: </strong>;
    expect(wrapper.contains(countryLife)).toEqual(true);
    const countrySatisfaction = <strong>Life Satisfaction Rating: </strong>;
    expect(wrapper.contains(countrySatisfaction)).toEqual(true);
    const countryHomicide = <strong>Homicide rate: </strong>;
    expect(wrapper.contains(countryHomicide)).toEqual(true);
    const countryHours = <strong>Employees Working Long Hours Rate: </strong>;
    expect(wrapper.contains(countryHours)).toEqual(true);
    expect(toJson(wrapper)).toMatchSnapshot();
});

const mockNews = {
    attributes: {
        title: "Big News",
        language: "English",
        author: "Yes",
        description: "Big news going on",
        source: "ESPN",
        image: "https://flagcdn.com/au.svg",
        url: "https://www.espn.com/",
        category: "sports",
        charities: [
            {
                id: "Red Cross",
                type: "charities"
            }
        ],
        country: [
            {
                id: "USA",
                type: "country"
            }
        ]
    }
};

// test 7 for news instance rendering properly
test("render News Instance", () => {
    const wrapper = shallow(<NewsInstance news={mockNews.attributes} />);
    expect(toJson(wrapper)).toMatchSnapshot();
});

// test 8 to check news properties
test("render News Instance", () => {
    const wrapper = shallow(<NewsInstance news={mockNews.attributes} />);
    const newsLanguage = <strong>Language(s): </strong>;
    expect(wrapper.contains(newsLanguage)).toEqual(true);
    const newsAuthor = <strong>Author: </strong>;
    expect(wrapper.contains(newsAuthor)).toEqual(true);
    const newsDescription = <strong>Description: </strong>;
    expect(wrapper.contains(newsDescription)).toEqual(true);
    const newsSource = <strong>Source: </strong>;
    expect(wrapper.contains(newsSource)).toEqual(true);
    const newsCountry = <strong>Country: </strong>;
    expect(wrapper.contains(newsCountry)).toEqual(true);
    expect(toJson(wrapper)).toMatchSnapshot();
});

// test 9
test("render About", () => {
    const wrapper = shallow(<About />);
    expect(toJson(wrapper)).toMatchSnapshot();
});

// test 10
test("render CharityRouter", () => {
    const wrapper = shallow(<CharityRouter />);
    expect(toJson(wrapper)).toMatchSnapshot();
});

// test 11
test("render CountryRouter", () => {
    const wrapper = shallow(<CountryRouter />);
    expect(toJson(wrapper)).toMatchSnapshot();
});

// test 12
test("render NewsRouter", () => {
    const wrapper = shallow(<NewsRouter />);
    expect(toJson(wrapper)).toMatchSnapshot();
});

// test 13
test("render Searchbar", () => {
    const wrapper = shallow(<Searchbar />);
    expect(toJson(wrapper)).toMatchSnapshot();
});

const funding_ranges = [
        "<1000", "1000-10000", "10000-50000", "50000-100000", ">100000"
    ];

// test 14
test("render Filterable for funding ranges for charities", () => {
    const wrapper = shallow(<Filterable list={funding_ranges} name='funding' class='charity'/>);
    expect(toJson(wrapper)).toMatchSnapshot();
});

const goal_ranges = [
        "<10000", "10000-50000", "50000-100000", ">100000"
    ];

// test 15
test("render Filterable for goal ranges for charities", () => {
    const wrapper = shallow(<Filterable list={goal_ranges} name='funding' class='charity'/>);
    expect(toJson(wrapper)).toMatchSnapshot();
});

const sort_list = {
        "Funding - Asc": "funding-inc", 
        "Funding - Desc":"funding-dec", 
        "Approval Date - Asc":"approval_date-inc", 
        "Approval Date - Desc":"approval_date-dec", 
        "Donors - Asc":"total_donors-inc", 
        "Donors - Desc":"total_donors-dec"
    };

// test 16
test("render Sortable for sort list for charities", () => {
    const wrapper = shallow(<Sortable dict={sort_list} name="sort" class='charity'/>);
    expect(toJson(wrapper)).toMatchSnapshot();
});

const sort_list_2 = {
        "Population - Asc": "population-inc",
        "Population - Desc": "population-dec",
        "Life Expectancy - Asc": "life_expectancy-inc",
        "Life Expectancy - Desc": "life_expectancy-dec",
        "Education Rate - Asc": "education_rate-inc",
        "Education Rate - Desc": "education_rate-dec"
    };

// test 17
test("render Sortable for sort list for countries", () => {
    const wrapper = shallow(<Sortable dict={sort_list_2} name="sort" class='country'/>);
    expect(toJson(wrapper)).toMatchSnapshot();
});

const homicide_ranges = [
        "0-1", "1-2", "2-3", "3-4", "4-5", ">5"
    ];

// test 18
test("render Filterable for homicide ranges for countries", () => {
    const wrapper = shallow(<Filterable list={homicide_ranges} name="homicide_rate" class='country'/>);
    expect(toJson(wrapper)).toMatchSnapshot();
});

const education_ranges = [
        "<50", "50-60", "61-70", "71-80", "81-90", ">90"
    ];

// test 19
test("render Filterable for education ranges for countries", () => {
    const wrapper = shallow(<Filterable list={education_ranges} name="education_rate" class='country'/>);
    expect(toJson(wrapper)).toMatchSnapshot();
});

const sort_list_3 = {
        "Published At - Asc":"published_at-inc", "Published At	- Desc":"published_at-dec", "Country - Asc":"country-inc", 
        "Country - Desc":"country-dec", "Category - Asc":"category-inc", "Category - Desc":"category-dec"
    };

// test 20
test("render Sortable for sort list for news", () => {
    const wrapper = shallow(<Sortable dict={sort_list}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
});

const published_ranges = [
    "2022/01/01-2022/01/31", "2022/02/01-2022/02/28", "2022/03/01-2022/03/31", "2022/04/01-Present"
];

// test 21
test("render Filterable for published ranges for news", () => {
    const wrapper = shallow(<Filterable list={published_ranges} name={`published_at`}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
});

const language_ranges = [
    "en", "spa", "ger", "por", "chi", "hin"
];

// test 22
test("render Filterable for language ranges for news", () => {
    const wrapper = shallow(<Filterable list={language_ranges} name={`category`}/>);
    expect(toJson(wrapper)).toMatchSnapshot();
});
