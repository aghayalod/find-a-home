import React, { useState } from "react";
import { Dropdown, DropdownButton } from 'react-bootstrap';

export default function Filterable(props) {

    const [selected, setSelected] = useState("");

    const handleSelect = (key, event) => {
        if(props.dict){
            setSelected(props.dict[key]);
        } else {
            setSelected(key);
        }
        handleChange(key)
    };
    function handleChange(param) {
        if (props.list) {
            props.onFilter({ [props.name]: param });
        } else {
            props.onFilter({ [props.name]: param });
        }
    };

    if (props.list) {
        return (
            <div>
                <DropdownButton
                    id="dropdown-basic-button"
                    variant="info"
                    className="floatRight"
                    onSelect={handleSelect}
                    title={selected !== "" ? selected : props.list[0]}
                >
                    {props.list.map((item) => {
                        return (
                            <Dropdown.Item key={item} eventKey={item}>
                                {item}
                            </Dropdown.Item>
                        );
                    })}
                </DropdownButton>
            </div>
        )
    }
    else {

        let sortable_keys = Object.keys(props.dict);
        // console.log(sortable_keys)

        // var res = sortable_keys.sort(function(a, b) {
        //     return props.dict[b] - props.dict[a];
        //   })

        // res.forEach(function(e){
        //     console.log(props.dict[e]);
        // })
        return (
        <div>
            <DropdownButton
                id="dropdown-basic-button"
                variant="info"
                className="floatRight"
                onSelect={handleSelect}
                title={selected !== "" ? selected : props.dict[sortable_keys[0]]}
            >
                {sortable_keys.map((item) => {
                    return (
                        <Dropdown.Item key={item} eventKey={item}>
                            {props.dict[item]}
                        </Dropdown.Item>
                    );
                })}
            </DropdownButton>
        </div>
        )
    }
}

Filterable.defaultProps = {
    hook: function useDummy() {
        const [queryParameters, setParameters] = useState({});
        return { queryParameters, setParameters };
    }
}

