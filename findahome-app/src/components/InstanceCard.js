import "bootstrap/dist/css/bootstrap.min.css"
import {
    Card,
    ListGroup,
    Row,
    Col,
    ListGroupItem
} from 'react-bootstrap'
import {Link} from 'react-router-dom'

export function InstanceCard(props) {
    return (
        <Link to={props.link} style={{ textDecoration: "none" }} >
            <Card>
                <Card.Img variant="top" src={props.img} />
                <Card.Body>
                    <Card.Title>{props.title}</Card.Title>
                    <Card.Subtitle className="text-muted">{props.subtitle}</Card.Subtitle>
                    <Card.Text className="text-dark">
                    {props.text}
                    </Card.Text>
                </Card.Body>
                <ListGroup>
                    <ListGroupItem>{props.item1}</ListGroupItem>
                    <ListGroupItem>{props.item2}</ListGroupItem>
                    <ListGroupItem>{props.item3}</ListGroupItem>
                </ListGroup>
            </Card>
        </Link>
    );
}

function CardTable(props) {
    return (
        <Row xxl={props.col}s>
            {props.cards.map((card, idx) => (
                <Col key={idx}>
                    {card}
                </Col>
            ))}
        </Row>
    );
}

CardTable.defaultProps = {
    col: "5"
}

export {CardTable};