import { Spinner } from 'react-bootstrap'

export default function Loader() {
    return (
        <Spinner animation="border" role="status"  style={{ width: '9rem', height: '9rem' }}>
        </Spinner>
    )
}