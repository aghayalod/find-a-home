import React, {useState, useEffect, useRef} from 'react';
import {Map, View} from "ol";
import XYZ from "ol/source/XYZ"
import "ol/ol.css"
import { fromLonLat } from "ol/proj";
import TileLayer from "ol/layer/Tile";
import "../App.css"

function CountryMap(props) {
    // set state
    const [, setMap] = useState()
    const [, setFeaturesLayer ] = useState()

    // get ref to div element to render to
    const mapElement = useRef()

    // Initial layer
    useEffect(() => {
        const initialLayer = new TileLayer({
            source: new XYZ({
              url: 'https://basemap.nationalmap.gov/arcgis/rest/services/USGSTopo/MapServer/tile/{z}/{y}/{x}',
            })
          })

        // create map
        const initialMap = new Map({
            target: mapElement.current,
            layers: [
                initialLayer
            ],
            view: new View({
                center: fromLonLat(props.coords),
                zoom: props.zoom
            })
        })

        setMap(initialMap)
        setFeaturesLayer(initialLayer)

    },[props.coords, props.zoom])

    return (
        <div ref={mapElement} style={{height: "400px",width:"100%"}}></div>
    )
}

CountryMap.defaultProps = {
    zoom: 7
}

export {CountryMap}