import React, { useState } from "react";
import { Dropdown, DropdownButton } from 'react-bootstrap';

export default function Sortable(props) {

    const [selected, setSelected] = useState({});

    const handleSelect = (key, event) => {
        setSelected({ key, value: event.target.value });
        console.log(key);
        handleChange(key)
    };

    function handleChange(param) {
        console.log(props.dict)
        props.onSort({ [props.name]: props.dict[param] });

    };

    let sortable_keys = Object.keys(props.dict);

    return (
        <DropdownButton
            id="dropdown-basic-button"
            variant="info"
            className="floatRight"
            onSelect={handleSelect}
            title={selected?.key || sortable_keys[0]}
        >
            {sortable_keys.map((item) => {
                return (
                    <Dropdown.Item key={item} eventKey={item}>
                        {item}
                    </Dropdown.Item>
                );
            })}
        </DropdownButton>
    )
}


Sortable.defaultProps = {
    hook: function useDummy() {
        const [queryParameters, setParameters] = useState({});
        return { queryParameters, setParameters };
    },
    name: "sort"
}

