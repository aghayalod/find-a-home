import React from "react";
import {complete_data} from './AboutDataStructures.js';
import axios from "axios";
import MemberCard from "./cards/MemberCard";
import StatsCard from "./cards/StatsCard";
import ToolsCard from "./cards/ToolsCard";
import ApiCard from "./cards/ApiCard";
import InfoCard from "./cards/InfoCard";
import styles from './About.module.css';
import Loader from '../../components/Loader';
import {
    Row,
    Col,
    Container,
} from 'react-bootstrap'


// Matching names to index complete.data.team_members
const names_dict = {
    "Avi K Ghayalod": 0,
    "Avi Ghayalod": 0,
    "Anish Roy": 1,
    "Anthony Chhang": 2,
    "Isaac Adams": 3,
    "Mya Mahaley": 4,
    "myamahaley": 4,
};


export default class About extends React.Component{
    // Each Index corresponds with names_dict convenctions
    // Ex: member_issues[4] holds the number of issues that Mya was assigned.
    // Ex: member_committs[3] holds the number of commits that Isaac made.
    // Ex: member_tests[2] holds the number of test that Anthony made
    constructor(props)
    {
        super(props);
        this.state = {
            loaded: false,
            member_issues: [0,0,0,0,0],
            issues_sum: 0,
            member_commits: [0,0,0,0,0],
            commits_sum: 0,
            member_tests: [41,0,12,0,0],
            tests_sum: 63,            
        }; 
    } 

    async update_commits() {
         // Changes to URL accredited to Feast for Friends
        // Previous URL was not giving access to all commits
        // https://gitlab.com/ZauniteKoopa/cs373-web-application/-/blob/AboutPage/Frontend/src/Components/AboutPage/AboutPage.js
        let url = 'https://gitlab.com/api/v4/projects/34068016/repository/commits?per_page=2000';
        let commit_list = [0,0,0,0,0];
        let commit_count = 0;  

  
        // Getting all commits for current branch
        let commits_response = await axios.get(url);
        let commits = commits_response.data;

        // Looping through commits of current branch, updating counts
        for(let i = 0; i < commits.length; i++){
            let cur_name = commits[i].committer_name;

            //index associated with name 
            let cur_member = names_dict[cur_name];
            if(cur_member != null){
                commit_list[cur_member] += 1;
            }
            commit_count += 1;
        }

        // Updating commit sums
        this.setState({commits_sum: commit_count, member_commits: commit_list});
    }

    async update_issues() {
        // Changes to URL accredited to Feast for Friends
        // Previous URL was not giving access to all issues
        // https://gitlab.com/ZauniteKoopa/cs373-web-application/-/blob/AboutPage/Frontend/src/Components/AboutPage/AboutPage.js
        let url = 'https://gitlab.com/api/v4/projects/34068016/issues?per_page=2000';
        let issue_list = [0,0,0,0,0];
        let issue_count = 0;  
        
        // Getting all issues for current branch
        let issues_response = await axios.get(url);
        let issues = issues_response.data;
            
        // Looping through issues of current branch, updating counts
        for(let i = 0; i < issues.length; i++){
            let cur_assignee = issues[i].assignee;
            // Go through asignees and update issues
            if(cur_assignee != null) {
                let cur_name = cur_assignee.name;
                let cur_member = names_dict[cur_name];
                if(cur_member != null){
                    issue_list[cur_member] += 1;
                }
            }
            issue_count += 1
        }

        // Updating issues sums
        this.setState({issues_sum: issue_count, member_issues: issue_list});
    }
    
    
    // Actually fetching Data here
    async componentDidMount () {
        // Async function to get data
        Promise.All(await this.update_issues(), await this.update_commits(), this.setState({loaded: true}));
    }
    
   

    getMemberData(i) {
        let cur_member = complete_data.team_members[i];
        let cur_props = {
            key: cur_member.name,
            photo: cur_member.photo,
            name: cur_member.name,
            bio: cur_member.bio,
            responsibility: cur_member.major_responsibilities,
            num_commits: this.state.member_commits[i],
            num_issues: this.state.member_issues[i],
            num_tests: this.state.member_tests[i],
        }  
        return cur_props;   
    }

     // Main method to make a developer card
     getMemberCard(i) {
        let cur_card = MemberCard(this.getMemberData(i));   
        return cur_card;   
    }

    getToolsCard(i) {
        let cur = complete_data.tools[i];
        
        return ToolsCard({
            name: cur.name,
            purpose: cur.purpose,
            logo: cur.logo,
            link: cur.link
        });
    }

    getApiCard(i) {
        let cur = complete_data.apis[i];
        return ApiCard({
            title: cur.title,
            details: cur.details,
            url: cur.url
        })
    }

     render() {
        // Creating cards based on all members and their updated data.
        let cards = [0,0,0,0,0,0];
        for(let i = 0; i < 5; i++){
            cards[i] = this.getMemberCard(i);    
        }
        cards[5] = StatsCard({num_commits: this.state.commits_sum,
        num_issues: this.state.issues_sum, num_tests: this.state.tests_sum });

        let tools_cards = [0,0,0,0,0,0,0,0,0,0]
        for(let i = 0; i < 10; i++){
            tools_cards[i] = this.getToolsCard(i);    
        }

        let api_cards = [0,0,0,0,0,0,0,0]
        for (let i = 0; i < 8; i++){
            api_cards[i] = this.getApiCard(i);
        }
        
        
        return (
            <div className={styles.wrapper}>
                <Container>
                    <Row>
                        <h1 className={styles.title}><strong>About</strong></h1>
                    </Row>
                    <Row>
                        <p> </p>
                    </Row>
                    <Row>
                        <Col>{InfoCard({title: "Description", description:complete_data.description})}</Col>
                        <Col>{InfoCard({title: "Integration Finds", description:complete_data.integration})}</Col>
                    </Row>
                    <Row>
                        <p> </p>
                    </Row>
                    <Row>
                        <h1 className={styles.title}><strong>Our Team</strong></h1>
                    </Row>
                    {this.state.loaded ? (
                        <Container>
                            <Row>
                            <p> </p>
                            </Row>
                            <Row>
                                <div class="col">{cards[5]}</div>
                                <div class="col">{cards[0]}</div>
                                <div class="col">{cards[1]}</div>
                            </Row>
                            <Row>
                                <p> </p>
                            </Row>
                            <Row>
                                <p> </p>
                            </Row>
                            <Row>
                                <div class="col">{cards[2]}</div>
                                <div class="col">{cards[3]}</div>
                                <div class="col">{cards[4]}</div>
                            </Row>
                            <Row>
                                <p> </p>
                            </Row>
                        </Container>
                    ) : (
                        <Row>
                            <Container className={styles.flexbox}>
                                <Row>
                                    <Loader/>
                                </Row>       
                            </Container>
                        </Row>
                        
                    )}
                    
                    <Row>
                        <h1 className={styles.title}><strong>Our Tools</strong></h1>
                    </Row>
                    <Row>
                        <p> </p>
                    </Row>
                    <Row>
                        <div class = "col">{tools_cards[8]}</div>
                        <div class = "col">{tools_cards[5]}</div>
                    </Row>
                    <Row>
                        <p> </p>
                    </Row>
                    <Row>
                        <h1 className={styles.title}><strong>Additional Tools</strong></h1>
                    </Row>
                    <Row>
                        <p> </p>
                    </Row>
                    <Row>
                        <div class = "col">{tools_cards[0]}</div>
                        <div class = "col">{tools_cards[1]}</div>
                        <div class = "col">{tools_cards[2]}</div>
                        <div class = "col">{tools_cards[3]}</div>
                    </Row>
                    <Row>
                        <p> </p>
                    </Row>
                    <Row>
                        <p> </p>
                    </Row>
                    <Row>
                        <div class = "col">{tools_cards[4]}</div>
                        <div class = "col">{tools_cards[9]}</div>
                        <div class = "col">{tools_cards[6]}</div>
                        <div class = "col">{tools_cards[7]}</div>
                    </Row>
                    <Row>
                        <p> </p>
                    </Row>
                    <Row>
                        <h1 className={styles.title}><strong>APIs</strong></h1>
                    </Row>
                    <Row>
                        <p> </p>
                    </Row>
                    <Row>
                        <div class = "col">{api_cards[0]}</div>
                        <div class = "col">{api_cards[1]}</div>
                    </Row>
                    <Row>
                        <p> </p>
                    </Row>
                    <Row>
                        <p> </p>
                    </Row>
                    <Row>
                        <div class = "col">{api_cards[2]}</div>
                        <div class = "col">{api_cards[3]}</div>
                    </Row>
                    <Row>
                        <p> </p>
                    </Row>
                    <Row>
                        <p> </p>
                    </Row>
                    <Row>
                        <div class = "col">{api_cards[4]}</div>
                        <div class = "col">{api_cards[5]}</div>
                    </Row>
                    <Row>
                        <p> </p>
                    </Row>
                    <Row>
                        <p> </p>
                    </Row>
                    <Row>
                        <div class = "col">{api_cards[6]}</div>
                        <div class = "col">{api_cards[7]}</div>
                    </Row>
                </Container>
            </div>
        );
    }
}



