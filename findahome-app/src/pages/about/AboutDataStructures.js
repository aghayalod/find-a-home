import myaPhoto from './headshots/mya_headshot.jpg';
import aviPhoto from './headshots/avi_headshot.jpg';
import anthonyPhoto from './headshots/anthony_headshot.jpg';
import isaacPhoto from './headshots/isaac_headshot.jpg';
import anishPhoto from './headshots/anish_headshot.jpg';

import aws_amplify_logo from './logos/aws_amplify_logo.png';
import discord_logo from './logos/discord_logo.png';
import docker_logo from './logos/docker_logo.png';
import gitlab_logo from './logos/gitlab_logo.png';
import namecheap_logo from './logos/namecheap_logo.png';
import postman_logo from './logos/postman_logo.png';
import prettier_logo from './logos/prettier_logo.png';
import react_bootstrap_logo from './logos/react_bootstrap_logo.png';
import react_logo from './logos/react_logo.png';
import vscode_logo from './logos/vscode_logo.png';

const anish = {
    name: "Anish Roy",
    photo: anishPhoto,
    bio: "I'm a junior CS major minoring in Business at UT Austin. I'm from Austin. In my free time, I enjoy playing basketball and hanging out with friends.",
    major_responsibilities: "Front-end | Phase III Leader",
    num_tests: 0,
};
const anthony = {
    name: "Anthony Chhang",
    photo: anthonyPhoto,
    bio: "I'm a third year CS major from Pflugerville, Texas. I enjoy playing sports (football, tennis), eating/hanging out with friends, and listening to music.",
    major_responsibilities: "Front-end",
    num_tests: 0,
};
const avi = {
    name: "Avi Ghayalod",
    photo: aviPhoto,
    bio: "I'm a third year CS major with a certificate in Applied Statistical Modeling. I'm from New Jersey and I enjoy spending my free time watching soccer and going bouldering/hiking.",
    major_responsibilities: "Back-end | Phase I Leader",
    num_tests: 0,
};
const isaac = {
    name: "Isaac Adams",
    photo: isaacPhoto,
    bio: "I’m a junior in computer science at UT Austin. I was born in Arizona, and I like to write music and play video games.",
    major_responsibilities: "Back-end | Phase II Leader",
    num_tests: 0,
};
const mya = {
    name: "Mya Mahaley",
    photo: myaPhoto,
    bio: "I'm a third year computer science major at UT Austin. I'm from Carrollton, Texas, and I like to read and I love to make playlists in my freetime!",
    major_responsibilities: "Front-end | Phase IV Leader",
    num_tests: 0,
};

//apis used
const global_living_api = {
    title: "Global Living API",
    url: "https://www.globalgiving.org/api/methods/get-all-projects-for-a-country/",
    details:"API used for charities"
};

const oecd_api = {
    title: "OECD API",
    url: "https://stats.oecd.org/index.aspx?DataSetCode=BLI",
    details: "API used for countrie's socioeconomic status "
};

const gitlab_api = {
    title: "GitLab API",
    url: "https://docs.gitlab.com/ee/api/",
    details: "Used to collect statistics about GitLab for about page"
}

const countries_cities_api = {
    title: "Countries & Cities API",
    url: "https://documenter.getpostman.com/view/1134062/T1LJjU52",
    details: "API used for retrieve flags and country statistics"
};

const rest_countries_api = {
    title: "REST Countries API",
    url: "https://restcountries.com/#api-endpoints-v2-all",
    details: "API used for basic country statistics"
};

const media_stack = {
    title: "Mediastack API",
    url: "https://mediastack.com/documentation",
    details: "Retrieving news stories and publishing information."
};

const clearbit = {
    title: "Clearbit Logo API",
    url: "https://clearbit.com/blog/logo",
    details: "Used for retrieving logos of companies"
}
const openlayers = {
    title: "OpenLayers Map API",
    url: "https://openlayers.org/",
    details: "API Used for retrieving logos of companies"
}

const tools_list = [
    {name: "React", purpose: "Used for the front-end of the website", 
    link: "https://reactjs.org/", logo: react_logo}, 

    {name: "React-bootstrap", purpose: "Component and CSS library for React", 
    link: "https://react-bootstrap.github.io/", logo: react_bootstrap_logo}, 

    {name: "Docker", purpose: "Used to create the MakeFile", 
    link: "https://www.docker.com/", logo: docker_logo}, 

    {name: "Prettier", purpose: "Used to format code", 
    link: "https://prettier.io/", logo: prettier_logo},

    {name: "Discord", purpose: "Used for communication", 
    link: "https://discord.com/", logo: discord_logo}, 

    {name: "Postman", purpose: "Wrote API documentation and for testing RESTful APIs", 
    link: "https://documenter.getpostman.com/view/19780100/UVksKZBW", logo: postman_logo},

    {name: "Namecheap", purpose: "Used for acquiring the domain needed",
    link: "https://www.namecheap.com/", logo: namecheap_logo},

    {name: "AWS Amplify", purpose: "Used for hosting and maintaining website", 
    link: "https://aws.amazon.com/amplify/", logo: aws_amplify_logo},

    {name: "GitLab", purpose: "Used for version control as well as CI/CD management", 
    link: "https://gitlab.com/aghayalod/find-a-home", logo: gitlab_logo},
    
    {name: "VSCode", purpose: "Preferred IDE that was used", 
    link: "https://code.visualstudio.com/", logo: vscode_logo}
];


export const complete_data = {
    description: "FindaHome is a project based around helping individuals looking for asylum or as refugees find information about the countries they are considering moving to. This includes social welfare information as well as general information regarding the country’s asylum status.",
    integration: "While developing our Countries, Charities, and News Models, we were able to more adequately understand the various data points that one may consider when moving to a new country. We are hoping to develop a deeper understanding of how those factors impact eachother.",
    team_members: [avi, anish, anthony, isaac, mya],
    total_commits: 0,
    total_issues: 0,
    total_tests: 0,
    apis: [global_living_api, oecd_api, countries_cities_api, rest_countries_api, media_stack, clearbit, openlayers, gitlab_api],
    tools: tools_list    
};


