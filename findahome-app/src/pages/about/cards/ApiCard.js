/* CSS Styling adapted from Texas Votes 
(https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/views/About/*/

import "bootstrap/dist/css/bootstrap.min.css"
import styles from "./Card.module.css"
import { Card} from "antd"
const {Meta} = Card

export default function ApiCard(props) {
    return (
		<a href={props.url}>
            <Card bordered={true} 
				className={styles.StatsCard}
				hoverable={true}
				// Match height of other components
				bodyStyle={{
					alignItems: "stretch",
					height: "100%",
					display: "flex",
					flexDirection: "column",
				}}>
            <a href={props.url}><h1 style={{ fontSize: '2em'}}><strong>{props.title}</strong></h1></a>          
            <Meta title={<strong>{props.details}</strong>}/>
            </Card>
		</a>
    );
}