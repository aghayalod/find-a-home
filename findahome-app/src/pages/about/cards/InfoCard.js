/* CSS Styling adapted from Texas Votes 
(https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/views/About/ */

import "bootstrap/dist/css/bootstrap.min.css"
import styles from "./Card.module.css"
import { Card} from "antd"


const {Meta} = Card

export default function InfoCard(props) {
    return (
        <Card bordered={true} className={styles.StatsCard}>
            <p> </p>
            <Meta title={<h3><strong>{props.title}</strong></h3>} description={props.description} className={styles.meta} />
            <p> </p>
        </Card>
    );
}