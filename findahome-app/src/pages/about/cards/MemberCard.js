import "bootstrap/dist/css/bootstrap.min.css"

import React from "react"
import { Card, Typography, Divider } from "antd"
import styles from "./Card.module.css"

const { Meta } = Card


/*
    This file defines a card that displays each developer
    on the About page. Cards have a circular image, name, 
    and description.
*/



export default function MemberCard(props){
    return (
        <Card bordered={true} cover={<img className={styles.circle_headshot} src={props.photo} alt={""}/>}
				className={styles.card}
				// Match height of other components
				bodyStyle={{
					alignItems: "stretch",
					height: "100%",
					display: "flex",
					flexDirection: "column",
				}}>
                <p> </p>
                
                <h3><strong>{props.name}</strong></h3>
                <div className={styles.gitStats}>
                    <div className={styles.gitStat}>
                        <div>{<strong>{props.num_commits}</strong>}</div>
                        <div>Commits</div>
                        <p> </p>
		            </div>
                    <div className={styles.gitStat}>
                        <div>{<strong>{props.num_issues}</strong>}</div>
                        <div>Issues</div>
                        <p> </p>
		            </div>
                    <div className={styles.gitStat}>
                        <div>{<strong>{props.num_tests}</strong>}</div>
                        <div>Tests</div>
                        <p> </p>
		            </div>
				</div>
                <p> </p>
				<Meta title={<strong>{props.responsibility}</strong>} description={props.bio} className={styles.meta} />
				
			</Card>

    );
}

