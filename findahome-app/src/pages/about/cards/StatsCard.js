/* CSS Styling adapted from Texas Votes 
(https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/views/About/*/

import "bootstrap/dist/css/bootstrap.min.css"
import styles from "./Card.module.css"
import { Card, Divider } from "antd"
import {
    GitlabOutlined
  } from '@ant-design/icons';

import {
    Row,
    Col,
} from 'react-bootstrap'

export default function StatsCard(props) {
    return (
            <Card bordered={true} 
				className={styles.StatsCard}
				// Match height of other components
				bodyStyle={{
					alignItems: "stretch",
					height: "100%",
					display: "flex",
					flexDirection: "column",
				}}
			>
                <p> </p>
                <p> </p>
                <h1 className={styles.title}>Statistics Overview</h1>
                <p> </p>
                <Row>
                    <Col><strong><GitlabOutlined style={{ fontSize: '3em'}}/></strong></Col>
                </Row>
                <p> </p>
                <p> </p>
                <h2>{props.num_commits}</h2>
                <h5>Commits</h5>
                <h2>{props.num_issues}</h2>
                <h5>Issues</h5>
                <h2>{props.num_tests}</h2>
                <h5>Tests</h5>
            </Card>
    );
}