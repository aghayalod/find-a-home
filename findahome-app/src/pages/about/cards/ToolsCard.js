/* CSS Styling adapted from Texas Votes 
(https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/src/views/About/ */

import "bootstrap/dist/css/bootstrap.min.css"
import styles from "./Card.module.css"
import { Card} from "antd"



const {Meta} = Card

export default function ToolsCard(props) {
    return (
                <a href={props.link}>
                    <Card bordered={true}
                        cover={<img className={styles.tool_image} src={props.logo} alt={""}/>}
                        hoverable={true}
                        className={styles.card}
                        
                        // Match height of other components
                        bodyStyle={{
                            alignItems: "stretch",
                            height: "100%",
                            display: "flex",
                            flexDirection: "column",}}>
                        <Meta title={<strong>{props.name}</strong>} description={props.purpose} className={styles.meta_tool} />
                    </Card>
                </a>
    );
}