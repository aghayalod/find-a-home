import React from "react";
import { Card } from "antd"
import card_styles from "../InstanceCard.module.css";
import styles from "../Instance.module.css";
import {
    Row,
    Col,
} from 'react-bootstrap'

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';

class CharityInstance extends React.Component {
    render() {
        
        const charity = this.props.charity;
        let make_title = (id) => {
            const words = id.split("-");
            for (let i = 0; i < words.length; i++) {
                words[i] = words[i][0].toUpperCase() + words[i].substr(1) + " ";
            }
            return(words);
        }
        

        let addr = charity.address.split(", ");
        let country_name = addr[addr.length -1];


        const country = this.props.charity.countries;
        let country_link = <h4><strong>General Information: </strong>N/A</h4>
        if (country != null && country[0] != null){
            let link = "/country/" + country[0].id;
            country_link = <h4><strong>General Information: </strong><a href={link}>{country_name}</a></h4>
        }



        const pagination_news = paginationFactory({
            page: 1,
            sizePerPage: 4,
            lastPageText: '>>',
            firstPageText: '<<',
            nextPageText: '>',
            prePageText: '<',
            alwaysShowAllBtns: true,
            hideSizePerPage: true
        });

       
        const news_columns = [
            {
                dataField: 'name',
                text: 'Related News Articles',
                formatter: (cell) => (
                    <a href={`/news/${cell}`}> {make_title(cell)} </a>
                )
            }
        ]

        const news = this.props.charity.news;
        
        let news_ids = Array(news.length);
        for(let i = 0; i < news.length; i++){
            news_ids[i] = {
                name: news[i].id
            }
        }

        let news_table_card;
        if(news_ids.length === 0){
            news_table_card =  <h4><strong>No Relevant News</strong></h4>
        }
        else{
            news_table_card= <BootstrapTable bootstrap4 keyField='name' data={news_ids} columns={news_columns} pagination={pagination_news} />
        }

        let themes = this.props.charity.themes;
        let themes_string = "";
        for(let i = 0; i < themes.length -1; i++){
            themes_string += themes[i].name + ", ";
        }
        if(themes.length > 0){
            themes_string += themes[themes.length -1].id;
        }
        return (
            <div className={styles.wrapper}>
                <h1 className={styles.title}><strong>{charity.name}</strong></h1>
                <p> </p>
                <p> </p>
                <Row>
                        <Card bordered={true} className={card_styles.card} 
                            bodyStyle={{
					        alignItems: "stretch",
					        height: "100%",
					        display: "flex",
					        flexDirection: "column",
				        }} >
                            <p> </p>
                            <Row>
                                <Col><h3><strong>{charity.organization}</strong></h3></Col>
                            </Row>
                            <Row>
                                <Col><h4 className={styles.description}><strong>Description: </strong>{charity.activities}</h4></Col>
                            </Row>
                            <p> </p>
                         </Card>
                    </Row>

                    <Row>
                        <Col>
                            <Card bordered={true} cover ={<img src={charity.image} alt={"No Img Available"}className={card_styles.charity_image}/>} 
                            className={card_styles.card} bodyStyle={{
                                alignItems: "stretch",
                                height: "100%",
                                display: "flex",
                                flexDirection: "column",
                            }}>
                            <Row>
                                <Col><h4><strong>Learn More At: </strong></h4></Col>
                            </Row>
                            <Row>
                                <h6><a href={`${charity.url}`}>{charity.url}</a></h6>
                            </Row>
                            </Card>
                        </Col>
                        <Col>
                        <Card bordered={true} className={card_styles.card}>
                            <Row>
                                <Col><h4 className={styles.description}><strong>Address: </strong>{charity.address}</h4></Col>
                            </Row>
                            <Row>
                                <Col><h4><strong>Current Funding: </strong>${charity.funding}</h4></Col>
                            </Row>
                            <Row>
                                <Col><h4><strong>Goal: </strong>${charity.goal}</h4></Col>
                            </Row>
                            <Row>
                                <Col><h4><strong>Total Donations: </strong>{charity.total_donations}</h4></Col>
                            </Row>
                            <Row>
                                <Col><h4><strong>Approval Date: </strong>{charity.approval_date}</h4></Col>
                            </Row>
                            <Row>
                                <Col><h4><strong>Status: </strong>{charity.status}</h4></Col>
                            </Row>
                            <Row>
                                <Col><h4><strong>Themes: </strong>{themes_string}</h4></Col>
                            </Row>
			            </Card>
                        </Col>
                    </Row>
                    <Row>
                        <p> </p>
                        <p></p>
                    </Row>
                    <Row>
                        <h2 className={styles.subtitle}><strong>Are you thinking about moving to {country_name}?</strong></h2>
                    </Row>    
                    <Row>
                        <p> </p>
                        <p></p>
                    </Row>
                    <Row>
                        <Col><Card bordered={true} className={card_styles.card}>{country_link}</Card></Col>
                        <Col><Card bordered={true} className={card_styles.card}>
                            {news_table_card}
                        </Card></Col>
                    </Row>    
            </div>
        );
    }
}

export default CharityInstance;