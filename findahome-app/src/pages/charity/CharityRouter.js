import React from 'react';
import { useParams } from 'react-router-dom';
import { useFetch } from "../../App";
import CharityInstance from './CharityInstance';
import styles from "../../Home.module.css"
import {Row, Container } from 'react-bootstrap';
import Loader from "../../components/Loader"

function CharityRouter() {
  let { id } = useParams();
  console.log(id);
  const [charityData, loading] = useFetch(`https://api.findahome.me/charities/${id}`);
  if (loading) {
    return (
      <div className={styles.wrapper}>
          <p> </p>
          <p> </p>
          <Container className={styles.flexbox}>
              <Row>
                  <Loader/>
              </Row>     
          </Container>
          <p> </p>
          <p> </p> 
      </div>
      
  );
  } 
  console.log(charityData)
  return <CharityInstance charity={charityData} />;
}

export default CharityRouter;
