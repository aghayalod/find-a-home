import React from "react";
import { CountryMap } from "../../components/Map";
import { Card } from "antd"
import card_styles from "../InstanceCard.module.css";
import styles from "../Instance.module.css";
import {
    Row,
    Col,
} from 'react-bootstrap'

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';


class CountryInstance extends React.Component {
    
    render() {
        const country = this.props.country;
        
        let make_title = (id) => {
            const words = id.split("-");
            for (let i = 0; i < words.length; i++) {
                words[i] = words[i][0].toUpperCase() + words[i].substr(1) + " ";
            }
            return(words);
        }

        const pagination_charity = paginationFactory({
            page: 1,
            sizePerPage: 5,
            lastPageText: '>>',
            firstPageText: '<<',
            nextPageText: '>',
            prePageText: '<',
            alwaysShowAllBtns: false,
            hideSizePerPage: true
        });

        const pagination_news = paginationFactory({
            page: 1,
            sizePerPage: 4,
            lastPageText: '>>',
            firstPageText: '<<',
            nextPageText: '>',
            prePageText: '<',
            alwaysShowAllBtns: false,
            hideSizePerPage: true
        });
        
        const charity_columns = [
            {
                dataField: 'name',
                text: 'Charity Names',
                formatter: (cell) => (
                    <a href={`/charity/${cell}`}> {make_title(cell)} </a>
                )
            }
        ]


        const charities = this.props.country.charities;
        let charity_ids = Array(charities.length);
        for(let i = 0; i < charities.length; i++){
            charity_ids[i] = {
                name: charities[i].id
            }
        }
        let charity_table;
        if(charity_ids.length === 0){
            charity_table = <h4><strong>No Relevant Charities</strong></h4>
        }
        else{
            charity_table = <BootstrapTable bootstrap4 keyField='name' data={charity_ids} columns={charity_columns} pagination={pagination_charity} />;
        }

        const news_columns = [
            {
                dataField: 'name',
                text: ' Articles',
                formatter: (cell) => (
                    <a href={`/news/${cell}`}> {make_title(cell)} </a>
                )
            }
        ]

        const news = this.props.country.news;

        let news_ids = Array(news.length);
        for(let i = 0; i < news.length; i++){
            news_ids[i] = {
                name: news[i].id
            }
        }
        let news_table_card;
        if(news_ids.length === 0){
            news_table_card =  <h4><strong>No Relavent News</strong></h4>
        }
        else{
            news_table_card= <BootstrapTable bootstrap4 keyField='name' data={news_ids} columns={news_columns} pagination={pagination_news} />
        }

        
        return (
           <div className={styles.wrapper}>
                <CountryMap coords={[country.long, country.lat]} zoom={4} />
                <h1 className={styles.title}><strong>{country.name}</strong></h1>
                <p> </p>
                <Row>
                    <Col><h2 className={styles.subtitle}><strong>General Information</strong></h2></Col>
                    <Col><h2 className={styles.subtitle}><strong>Quality of Life</strong></h2></Col>
                </Row>
                <p> </p>
                <p> </p>
                <Row>
                    <Col>
                        <Card bordered={true} cover={<img src={country.flag_img} alt={"No Img Available"} className={card_styles.tool_image}/>}
                        className={card_styles.card} >
                            <p> </p>
                            <Row>
                                <Col><h3><strong>Language(s): </strong>{country.language}</h3></Col>
                            </Row>
                            <Row>
                                <Col><h3><strong>Region: </strong>{country.region}</h3></Col>
                            </Row>
                            <Row>
                                <Col><h3><strong>Population: </strong>{country.population}</h3></Col>
                            </Row>
                            <Row>
                                <Col><h3><strong>Currency: </strong>{country.currency}</h3></Col>
                            </Row>
			            </Card>
                    </Col>
                    <Col>
                        <Card bordered={true} className={card_styles.card} >
                            <Row>
                                <Col><h5><strong>Dwellings without basic facilities: </strong>{country.dwellings_no_facilities_rate}</h5></Col>
                            </Row>
                            <Row>
                                <Col><h5><strong>Household Net Wealth: </strong>{country.household_net_worth}</h5></Col>
                            </Row>
                            <Row>
                                <Col><h5><strong>Educational Attainment: </strong>{country.education_rate}%</h5></Col>
                            </Row>
                            <Row>
                                <Col><h5><strong>Employment Rate: </strong>{country.employment_rate}%</h5></Col>
                            </Row>
                            <Row>
                                <Col><h5><strong>Voter Turnout: </strong>{country.voter_turnout}%</h5></Col>
                            </Row>
                            <Row>
                                <Col><h5><strong>Life Expectancy: </strong>{country.life_expectancy} years old</h5></Col>
                            </Row>
                            <Row>
                                <Col><h5><strong>Life Satisfaction Rating: </strong>{country.life_satisfaction_rating}/10</h5></Col>
                            </Row>
                            <Row>
                                <Col><p className={styles.description}><i>(A weighted-sum of different response categories based on people's rates of their current life relative to the best and worst possible lives for them on a scale from 0 to 10).</i></p></Col>
                            </Row>
                            <Row>
                                <Col><h5><strong>Homicide rate: </strong>{country.homicide_rate} deaths/100,000 people</h5></Col>
                            </Row>
                            <Row>
                                <Col><h5><strong>Employees Working Long Hours Rate: </strong>{country.long_hours_rate}%</h5></Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <p> </p>
                    <p> </p>
                </Row>
                <Row>
                    <h2 className={styles.subtitle}><strong>Are you thinking about moving to {country.name}?</strong></h2>
                </Row>
                <p> </p>
                <p> </p>
                <Row>
                    <Col>
                        <Card bordered={true} className={card_styles.card}>
                            <Row><h4><strong>Relavent Charities </strong></h4></Row>
                            <Row>{charity_table}</Row>
                        </Card>
                    </Col>
                    <Col>
                        <Card bordered={true} className={card_styles.card}>
                            <Row><h4><strong>Current Events </strong></h4></Row>
                            <Row>{news_table_card}</Row>
                        </Card>
                    </Col>
                
                </Row>
                
                
                
            </div >
            

        );
    }
}

export default CountryInstance;