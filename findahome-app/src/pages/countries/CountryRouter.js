import React from 'react';
import { useParams } from 'react-router-dom';
import { useFetch } from "../../App";
import CountryInstance from './CountryInstance';
import styles from "../../Home.module.css";
import {Row, Container } from 'react-bootstrap';
import Loader from "../../components/Loader"

function CountryRouter() {
  let { id } = useParams();
  const [countryData, loading] = useFetch(`https://api.findahome.me/countries/${id}`);
  if (loading || countryData == null) {
    return (
      <div className={styles.wrapper}>
          <p> </p>
          <p> </p>
          <Container className={styles.flexbox}>
              <Row>
                  <Loader/>
              </Row>     
          </Container>
          <p> </p>
          <p> </p> 
      </div>
      
  );
  } 
  return <CountryInstance country={countryData} />;
}

export default CountryRouter;
