import React, { Fragment, useEffect, useState } from "react";

import { Button, Col, Row, Container } from 'react-bootstrap';
import { PAGE_SIZE } from '../../constants';

import Table from "react-bootstrap/Table";
import { useNavigate, useSearchParams, createSearchParams } from "react-router-dom";

import { useFetch } from "../../App";
import Filterable from "../../components/Filterable";
import Sortable from "../../components/Sortable";

import Searchbar from "../../search/Searchbar.js"
import Highlighter from "react-highlight-words"
import styles from "../../Home.module.css"

import Loader from "../../components/Loader"
import 'antd/dist/antd.dark.css';

export function CharityTable(props) {
    const navigate = useNavigate();

    const sort_list = {
        "Funding - Asc": "funding-inc",
        "Funding - Desc": "funding-dec",
        "Approval Date - Asc": "approval_date-inc",
        "Approval Date - Desc": "approval_date-dec",
        "Donors - Asc": "total_donations-inc",
        "Donors - Desc": "total_donations-dec",
        "Address - (A-Z)": "address-inc",
        "Address - (Z-A)": "address-dec",
    };

    const goal_ranges = [
        "<10000", "10000-50000", "50000-100000", ">100000"
    ];

    const funding_ranges = [
        "<1000", "1000-10000", "10000-50000", "50000-100000", ">100000"
    ];

    const date_ranges = [
        "<2010/01/01", "2010/01/01-2013/12/31", "2013/01/01-2016/12/31", "2016/01/01-2019/12/31", "2019/01/01-2021/12/31",
        "2022/01/01-2022/03/31", ">2022/04/01"
    ];

    const donor_ranges = [
        "0-10", "10-100", "100-1000", "1000-5000", ">5000"
    ]

    const themes_dict = {
        'disability': 'Disability Rights',
        'disaster': 'Disaster Response',
        'ecdev': 'Economic Growth',
        'edu': 'Education',
        'endabuse': 'Ending Abuse',
        'env': 'Ecosystem Restoration',
        'gender': 'Gender Equality',
        'health': 'Physical Health',
        'housing': 'Safe Housing',
        'human': 'Ending Human Trafficking',
        'hunger': 'Food Security',
        'justice': 'Racial Justice',
        'lgbtq': 'LGBTQIA+ Equality',
        'mentalhealth': 'Mental Health',
        'refugee': 'Refugee Rights',
        'reproductive': 'Reproductive Health',
        'rights': 'Justice and Human Rights',
        'sport': 'Sport',
        'tech': 'Digital Literacy',
        'water': 'Clean Water',
        'wildlife': 'Wildlife Conservation',
    }
    function handleRowClick(charityName) {
        navigate("/charity/" + charityName);
    }
    const [params, setParams] = useState(makeParams(props.params, null));

    function paramsEqual(p1, p2) {
        if (!p1 && !p2) {
            return true;
        }
        if (!p1 || !p2) {
            return false;
        }
        const keys1 = Object.keys(p1);
        const keys2 = Object.keys(p2);

        if (keys1.length !== keys2.length) {
            return false;
        }

        for (let key of keys1) {
            //console.log("key", key ,"p1[key]", p1[key], "p2[key]", p2[key]);
            if (p1[key] !== p2[key]) {
                return false;
            }
        }

        return true;
    }
    // Constructs a params object by merging newParams and params
    function makeParams(newParams, params) {
        let p = {};
        if (params) {
            p = { ...params };
        }
        for (let key in newParams) {
            if (newParams[key]) {
                p[key] = newParams[key];
            } else {
                delete p[key];
            }
        }
        p["per_page"] = PAGE_SIZE;
        if (!p["page"]) {
            p["page"] = 1;
        }
        return p;
    }

    const [filters, setFilters] = useState({});

    // Adds the given filter parameter to the filters object
    function addFilter(filter) {
        let f = { ...filters, ...filter };
        if (!paramsEqual(f, filters)) {
            setFilters({ ...filters, ...filter });
        }
    }

    // Adds the given params to the params object
    function addParams(newParams) {
        let p = makeParams(newParams, params);
        if (!paramsEqual(p, params)) {
            //console.log("Params not equal: ", p, params);
            setParams(p);
        }
    }


    // Sets the page in the params object
    function setPage(page) {
        addParams({ page: page });
    }

    useEffect(() => {
        if (props.params) {
            // Reset page to 1 if search params change
            let p = { ...props.params, page: 1 };
            addParams(p);
        }
    }, [props.params]);

    // Updates the params object with the current filters
    function applyFiltersAndSort() {
        // Reset page to 1 if filter settings change
        let f = { ...filters, page: 1 }
        addParams(f);
    }
    function removeFiltersAndSort() {
        setFilters({ page: 1 });
        setParams({ page: 1 });
    }

    function findAndBoldSearch(text, searchWord){
        if(searchWord === null){
            return <p>{text}</p>
        }
        return   <Highlighter
        searchWords={[searchWord]}
        autoEscape={true}
        highlightClassName={styles.color_highlight}
        textToHighlight={text}></Highlighter>
    }

    // Makes the query url pased on params and props.url 
    function makeQuery() {
        let url = new URL(props.url);
        if (params) {
            url.search = new URLSearchParams(params).toString();
        }
        return url.toString();
    }
    useEffect(() => {
        let q = makeQuery();
        //console.log(q, " = ", query, "?");
        if (q !== query) {
            setQuery(q)
        }
    }, [props.url, params])

    const [query, setQuery] = useState(makeQuery());

    const [charityData, loading] = useFetch(query);

    let table = null;
    if (loading) {
        table = (
            <div className={styles.wrapper}>
                <p> </p>
                <p> </p>
                <Container className={styles.flexbox}>
                    <Row>
                        <Loader/>
                    </Row>     
                </Container>
                <p> </p>
                <p> </p> 
            </div>
            
        );
    } else {
        const charities = charityData.result;
        const totalPages = Math.floor(charityData.count / PAGE_SIZE) + (charityData.count % PAGE_SIZE !== 0 ? 1 : 0);

        const divStyle = {
            display: 'flex',
            alignItems: 'center'
        };



        table = (
            <Fragment>
                <Table striped bordered hover variant="dark">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Address</th>
                            <th>Funding</th>
                            <th>Goal</th>
                            <th>Approval date</th>
                            <th>Total Donors</th>
                            <th>Themes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <>
                            {Object.keys(charities).map(i => {
                                var themes  = "";
                                charities[i].themes.forEach(function(e, index){
                                    themes = themes + (e.name) + ", ";
                                });
                                themes = themes.substring(0, themes.length - 2)
                                return (<tr onClick={() => handleRowClick(charities[i].id)} key={charities[i].name}>
                                    <td>{findAndBoldSearch(charities[i].name, props.params['search'])}</td>
                                    <td>{findAndBoldSearch(charities[i].address, props.params['search'])}</td>
                                    <td>{charities[i].funding}</td>
                                    <td>{charities[i].goal}</td>
                                    <td>{charities[i].approval_date}</td>
                                    <td>{charities[i].total_donations}</td>
                                    <td>{themes}</td>
                                </tr>)
                            })}
                        </>
                    </tbody>
                </Table>
                <div style={divStyle}>
                    <Button
                        disabled={params["page"] === 1}
                        onClick={() => setPage(1)}>
                        {'First'}
                    </Button>
                    <Button
                        disabled={params["page"] === 1}
                        onClick={() => setPage(params["page"] - 1)}>
                        {'Previous'}
                    </Button>
                    <h4>Page {params["page"]} of {totalPages}</h4>
                    <Button
                        disabled={params["page"] >= totalPages}
                        onClick={() => setPage(params["page"] + 1)}>
                        {'Next'}
                    </Button>
                    <Button
                        disabled={params["page"] >= totalPages}
                        onClick={() => setPage(totalPages)} >
                        {'Last'}
                    </Button>
                </div>
            </Fragment>
        );
    }

    return (
        <div className={styles.wrapper} >
           <Container>
                <Row>
                    <Col></Col>
                    <Col><Button onClick={() => applyFiltersAndSort()}>Apply Filters</Button></Col>
                    <Col><Button onClick={() => removeFiltersAndSort()}>No Filters</Button></Col>
                </Row>
                <Row>
                    <Col>
                        <h6>Funding</h6>
                        <Filterable list={funding_ranges} name='funding' onFilter={addFilter}></Filterable>
                    </Col>
                    <Col>
                        <h6>Goal</h6>
                        <Filterable list={goal_ranges} name='goal' onFilter={addFilter}></Filterable>
                    </Col>
                    <Col>
                        <h6>Dates</h6>
                        <Filterable list={date_ranges} name='approval_date' onFilter={addFilter}></Filterable>

                    </Col>
                    <Col>
                        <h6># Donors</h6>
                        <Filterable list={donor_ranges} name='total_donations' onFilter={addFilter}></Filterable>
                    </Col>
                    <Col>
                        <h6>Themes</h6>
                        <Filterable dict={themes_dict} name='themes' onFilter={addFilter}></Filterable>
                    </Col>
                    <Col>
                        <h6>Sort</h6>
                        <Sortable dict={sort_list} onSort={addFilter}></Sortable>
                    </Col>
                </Row>
                <Row>
                    <p> </p>
                    <p> </p>
                </Row>
            </Container>

            {table}

        </div>
    );
}

CharityTable.defaultProps = {
    url: "https://api.findahome.me/charities"
}

function Charity(props) {
    const navigate = useNavigate();
    const [searchParams, setSearchParams] = useSearchParams();

    let search = searchParams.get("q");
    let params = { search: search };

    function onSearch(query) {
        if (!query) {
            navigate("/charity");
            return;
        }
        navigate({
            pathname: "/charity",
            search: `?${createSearchParams({ q: query })}`
        });
    }

    return (
        <div className={styles.wrapper} >
            <h1 className={styles.title}><strong>Charities</strong></h1>
            <Searchbar onSearch={onSearch} value={search} />
            <CharityTable params={params} />
        </div>
    );

}

export default Charity;