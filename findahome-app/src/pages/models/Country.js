import React, { useState, useEffect, Fragment } from "react";

import Table from "react-bootstrap/Table";
import { Button, Row, Col, Container } from 'react-bootstrap';
import { PAGE_SIZE } from '../../constants';
import { useNavigate, useSearchParams, createSearchParams } from "react-router-dom";

import Searchbar from "../../search/Searchbar.js"


import { useFetch } from "../../App";
import Filterable from "../../components/Filterable";
import Sortable from "../../components/Sortable";

import Highlighter from "react-highlight-words"
import styles from "../../Home.module.css"
import Loader from "../../components/Loader"

export function CountryTable(props) {
    const navigate = useNavigate();

    const sort_list = {
        "Population - Asc": "population-inc",
        "Population - Desc": "population-dec",
        "Life Expectancy - Asc": "life_expectancy-inc",
        "Life Expectancy - Desc": "life_expectancy-dec",
        "Education Rate - Asc": "education_rate-inc",
        "Education Rate - Desc": "education_rate-dec"
    };

    const population_ranges = [
        "<10000000", "10000000-25000000", "25000000-100000000", ">100000000"
    ];

    const life_ranges = [
        "<75", "75-78", "78-80", "80-83", ">83"
    ];


    const education_ranges = [
        "<50", "50-60", "61-70", "71-80", "81-90", ">90"
    ];

    const homicide_ranges = [
        "0-1", "1-2", "2-3", "3-4", "4-5", ">5"
    ];

    const household_net_worth_ranges = [
        "<100000", "100000-250000", "250000-500000", "500000-750000", ">750000"
    ]


    function handleRowClick(country) {
        navigate("/country/" + country);
    }

    const [params, setParams] = useState(makeParams(props.params, null));

    function makeParams(newParams, params) {
        let p = {};
        if (params) {
            p = { ...params };
        }
        for (let key in newParams) {
            if (newParams[key]) {
                p[key] = newParams[key];
            } else {
                delete p[key];
            }
        }
        p["per_page"] = PAGE_SIZE;
        if (!p["page"]) {
            p["page"] = 1;
        }
        return p;
    }

    // Checks if two params objects are equal
    function paramsEqual(p1, p2) {
        if (!p1 && !p2) {
            return true;
        }
        if (!p1 || !p2) {
            return false;
        }
        const keys1 = Object.keys(p1);
        const keys2 = Object.keys(p2);

        if (keys1.length !== keys2.length) {
            return false;
        }

        for (let key of keys1) {
            //console.log("key", key ,"p1[key]", p1[key], "p2[key]", p2[key]);
            if (p1[key] !== p2[key]) {
                return false;
            }
        }

        return true;
    }
    const [filters, setFilters] = useState({});


    // Adds the given filter parameter to the filters object
    function addFilter(filter) {
        let f = { ...filters, ...filter };
        if (!paramsEqual(f, filters)) {
            setFilters({ ...filters, ...filter });
        }
    }

    // Adds the given params to the params object
    function addParams(newParams) {
        console.log(newParams);

        let p = makeParams(newParams, params);
        if (!paramsEqual(p, params)) {
            //console.log("Params not equal: ", p, params);
            setParams(p);
        }
    }


    // Sets the page in the params object
    function setPage(page) {
        addParams({ page: page });
    }

    useEffect(() => {
        if (props.params) {
            // Reset page to 1 if search params change
            let p = { ...props.params, page: 1 };
            addParams(p);
        }
    }, [props.params]);
    // Updates the params object with the current filters
    function applyFiltersAndSort() {
        console.log(filters);

        // Reset page to 1 if filter settings change
        let f = { ...filters, page: 1 }
        addParams(f);
    }

    function findAndBoldSearch(text, searchWord){
        if(searchWord === null){
            return <p>{text}</p>
        }
        return   <Highlighter
        highlightClassName={styles.color_highlight}
        searchWords={[searchWord]}
        autoEscape={true}
        textToHighlight={text}></Highlighter>
    }


    function removeFiltersAndSort() {
        setFilters({ page: 1 });
        setParams({ page: 1 });
    }

    // Makes the query url pased on params and props.url 
    function makeQuery() {
        let url = new URL(props.url);
        if (params) {
            url.search = new URLSearchParams(params).toString();
        }
        return url.toString();
    }
    //console.log("Going to query this: " + generateQueryString());

    useEffect(() => {
        let q = makeQuery();
        //console.log(q, " = ", query, "?");
        if (q != query) {
            setQuery(q)
        }
    }, [props.url, params])

    const [query, setQuery] = useState(makeQuery());
    const [countryData, loading] = useFetch(query);

    let table = null;
    if (loading) {
        table = (
            <div className={styles.wrapper}>
                <p> </p>
                <p> </p>
                <Container className={styles.flexbox}>
                    <Row>
                        <Loader/>
                    </Row>     
                </Container>
                <p> </p>
                <p> </p> 
            </div>
            
        );
    } else {
        const countries = countryData.result;
        const totalPages = Math.floor(countryData.count / PAGE_SIZE) + (countryData.count % PAGE_SIZE !== 0 ? 1 : 0);

        const divStyle = {
            display: 'flex',
            alignItems: 'center'
        };

        console.log("rendering country");
        console.log("Page number returned from API: " + countryData.page_num)

        table = (
            <Fragment>
                <Table striped bordered hover variant="dark">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Population</th>
                            <th>Life Expectancy</th>
                            <th>Education Rate</th>
                            <th>Homicide Rate</th>
                            <th>Household Networth</th>
                        </tr>
                    </thead>
                    <tbody>
                        <>
                            {Object.keys(countries).map(i => {
                                return (<tr onClick={() => handleRowClick(countries[i].id)} key={countries[i].name}>
                                    
                                    <td>{findAndBoldSearch(countries[i].name, props.params['search'])}</td>
                                    <td>{countries[i].population}</td>
                                    <td>{countries[i].life_expectancy}</td>
                                    <td>{countries[i].education_rate}</td>
                                    <td>{countries[i].homicide_rate}</td>
                                    <td>{countries[i].household_net_worth}</td>
                                </tr>)
                            })}
                        </>
                    </tbody>
                </Table>
                <div style={divStyle}>
                    <Button
                        disabled={params["page"] === 1}
                        onClick={() => setPage(1)}>
                        {'First'}
                    </Button>
                    <Button
                        disabled={params["page"] === 1}
                        onClick={() => setPage(params["page"] - 1)}>
                        {'Previous'}
                    </Button>
                    <h4>Page {params["page"]} of {totalPages}</h4>
                    <Button
                        disabled={params["page"] >= totalPages}
                        onClick={() => setPage(params["page"] + 1)}>
                        {'Next'}
                    </Button>
                    <Button
                        disabled={params["page"] >= totalPages}
                        onClick={() => setPage(totalPages)} >
                        {'Last'}
                    </Button>
                </div>
            </Fragment>
        )
    }

    return (
        <div className={styles.wrapper}>

            <Container>
                <Row>
                    <Col></Col>
                    <Col><Button onClick={() => applyFiltersAndSort()}>Apply Filters</Button></Col>
                    <Col><Button onClick={() => removeFiltersAndSort()}>No Filters</Button></Col>
                </Row>
                <Row>
                    <p> </p>
                    <p> </p>
                </Row>
                <Row>
                    <Col>Population</Col>

                    <Col>Life Expectancy</Col>

                    <Col>Education Rate</Col>

                    <Col>Homicide Rate</Col>

                    <Col>Household Networth</Col>
                    <Col>Sort</Col>
                </Row>
                <Row>
                    <Col>
                        <Filterable list={population_ranges} name="population" onFilter={addFilter}></Filterable>
                    </Col>
                    <Col>
                        <Filterable list={life_ranges} name="life_expectancy" onFilter={addFilter}></Filterable>
                    </Col>
                    <Col>
                        <Filterable list={education_ranges} name="education_rate" onFilter={addFilter}></Filterable>
                    </Col>
                    <Col>
                        <Filterable list={homicide_ranges} name="homicide_rate" onFilter={addFilter}></Filterable>
                    </Col>
                    <Col>
                        <Filterable list={household_net_worth_ranges} name="household_net_worth" onFilter={addFilter}></Filterable>
                    </Col>
                    <Col>
                        <Sortable dict={sort_list} name="sort" onSort={addFilter}></Sortable>
                    </Col>
                </Row>
                <Row>
                <Row>
                    <p> </p>
                    <p> </p>
                </Row>
                </Row>
            </Container>

            {table}

        </div>
    );
}

CountryTable.defaultProps = {
    url: "https://api.findahome.me/countries"
}

function Country() {
    const navigate = useNavigate();
    const [searchParams, setSearchParams] = useSearchParams();

    let search = searchParams.get("q");
    let params = { search: search };

    function onSearch(query) {
        if (!query) {
            navigate("/country");
            return;
        }
        navigate({
            pathname: "/country",
            search: `?${createSearchParams({ q: query })}`
        });
    }

    return (
        <div className={styles.wrapper} >
            <h1 className={styles.title}><strong>Countries</strong></h1>
            <Searchbar onSearch={onSearch} value={search} />
            <CountryTable params={params} />
        </div>
    );

}

export default Country;