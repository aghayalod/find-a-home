import React, { useState, useEffect, Fragment } from "react";

import Table from "react-bootstrap/Table";
import { Button, Row, Col, Container } from 'react-bootstrap';
import { PAGE_SIZE } from '../../constants';
import { useNavigate, useSearchParams, createSearchParams } from "react-router-dom";

import { useFetch } from "../../App";
import Filterable from "../../components/Filterable";
import Sortable from "../../components/Sortable";

import Searchbar from "../../search/Searchbar.js"
import Highlighter from "react-highlight-words"
import index from "../../index.css"
import styles from "../../Home.module.css"
import Loader from "../../components/Loader"

export function NewsTable(props) {
    const navigate = useNavigate();

    const sort_list = {
        "Earliest": "published_at-inc", "Latest": "published_at-dec", "Country - (A-Z)": "country_id-inc",
        "Country - (Z-A)": "country_id-dec", "Category - (A-Z)": "category-inc", "Category - (Z-A)": "category-dec",
        "Author - (A-Z)": "author-inc", "Author - (Z-A)": "author-dec"
    };

    const published_ranges = [
        "2022/01/01-2022/01/31", "2022/02/01-2022/02/28", "2022/03/01-2022/03/31", "2022/04/01-Present"
    ];

    const language_ranges = [
        "en", "es", "de", "pt", "nl", "it", "fr", "no", "sv",
    ];

    const category_ranges = [
        'general', 'business', 'science', 'sports', 'technology', 'entertainment', 'politics', 'health',
    ]


    const country_ranges = [
        'USA', 'ITA', 'ESP', 'DEU', 'BRA', 'PRT', 'NLD', 'GBR', 'MEX', 'AUT', 'CAN', 'AUS', 'ZAF', 'CHE', 'NOR', 'SWE'
    ]

    const sources_ranges = [
        'americanbankingnews', 'infosurhoy', '4-traders', 'zazoom', 'CNN', 'economico', 'publico'
    ]

    function handleRowClick(news) {
        navigate("/news/" + news);
    }

    const [params, setParams] = useState(makeParams(props.params, null));

    // Constructs a params object by merging newParams and params
    function makeParams(newParams, params) {
        let p = {};
        if (params) {
            p = { ...params };
        }
        for (let key in newParams) {
            if (newParams[key]) {
                p[key] = newParams[key];
            } else {
                delete p[key];
            }
        }
        p["per_page"] = PAGE_SIZE;
        if (!p["page"]) {
            p["page"] = 1;
        }
        return p;
    }

    // Checks if two params objects are equal
    function paramsEqual(p1, p2) {
        if (!p1 && !p2) {
            return true;
        }
        if (!p1 || !p2) {
            return false;
        }
        const keys1 = Object.keys(p1);
        const keys2 = Object.keys(p2);

        if (keys1.length !== keys2.length) {
            return false;
        }

        for (let key of keys1) {
            //console.log("key", key ,"p1[key]", p1[key], "p2[key]", p2[key]);
            if (p1[key] !== p2[key]) {
                return false;
            }
        }

        return true;
    }

    const [filters, setFilters] = useState({});

    // Adds the given filter parameter to the filters object
    function addFilter(filter) {
        let f = { ...filters, ...filter };
        if (!paramsEqual(f, filters)) {
            setFilters({ ...filters, ...filter });
        }
    }

    function findAndBoldSearch(text, searchWord) {
        if (searchWord === null) {
            return <p>{text}</p>
        }
        return <Highlighter
            highlightClassName={styles.color_highlight}
            searchWords={[searchWord]}
            autoEscape={true}
            textToHighlight={text}></Highlighter>
    }


    // Adds the given params to the params object
    function addParams(newParams) {
        let p = makeParams(newParams, params);
        if (!paramsEqual(p, params)) {
            //console.log("Params not equal: ", p, params);
            setParams(p);
        }
    }

    // Sets the page in the params object
    function setPage(page) {
        addParams({ page: page });
    }

    useEffect(() => {
        if (props.params) {
            // Reset page to 1 if search params change
            let p = { ...props.params, page: 1 };
            addParams(p);
        }
    }, [props.params]);

    // Updates the params object with the current filters
    function applyFiltersAndSort() {
        // Reset page to 1 if filter settings change
        let f = { ...filters, page: 1 }
        addParams(f);
    }

    function removeFiltersAndSort() {
        setFilters({ page: 1 });
        setParams({ page: 1 });
    }

    // Makes the query url pased on params and props.url 
    function makeQuery() {
        let url = new URL(props.url);
        if (params) {
            url.search = new URLSearchParams(params).toString();
        }
        return url.toString();
    }

    useEffect(() => {
        let q = makeQuery();
        if (q !== query) {
            setQuery(q)
        }
    }, [props.url, params])

    const [query, setQuery] = useState(makeQuery());

    const [newsData, loading] = useFetch(query);

    let table = null;
    if (loading) {
        table = (
            <div className={styles.wrapper}>
                <p> </p>
                <p> </p>
                <Container className={styles.flexbox}>
                    <Row>
                        <Loader/>
                    </Row>     
                </Container>
                <p> </p>
                <p> </p> 
            </div>
            
        );
    } else {
        const news = newsData.result;
        const totalPages = Math.floor(newsData.count / PAGE_SIZE) + (newsData.count % PAGE_SIZE !== 0 ? 1 : 0);
        const divStyle = {
            display: 'flex',
            alignItems: 'center'
        };

        table = (
            <Fragment>
                <Table striped bordered hover variant="dark">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Category</th>
                            <th>Language</th>
                            <th>Published At</th>
                            <th>Country</th>
                        </tr>
                    </thead>
                    <tbody>
                        <>
                            {Object.keys(news).map(i => {
                                return (<tr onClick={() => handleRowClick(news[i].id)} key={news[i].title}>
                                    <td>{findAndBoldSearch(news[i].title, props.params['search'])}</td>
                                    <td>{findAndBoldSearch(news[i].author, props.params['search'])}</td>
                                    <td>{news[i].category}</td>
                                    <td>{news[i].language}</td>
                                    <td>{news[i].published_at}</td>
                                    <td>{news[i].country_id}</td>
                                </tr>)
                            })}
                        </>
                    </tbody>
                </Table>
                <div style={divStyle}>
                    <Button
                        disabled={params["page"] === 1}
                        onClick={() => setPage(1)}>
                        {'First'}
                    </Button>
                    <Button
                        disabled={params["page"] === 1}
                        onClick={() => setPage(params["page"] - 1)}>
                        {'Previous'}
                    </Button>
                    <h4>Page {params["page"]} of {totalPages}</h4>
                    <Button
                        disabled={params["page"] >= totalPages}
                        onClick={() => setPage(params["page"] + 1)}>
                        {'Next'}
                    </Button>
                    <Button
                        disabled={params["page"] >= totalPages}
                        onClick={() => setPage(totalPages)} >
                        {'Last'}
                    </Button>
                </div>
            </Fragment>
        );
    }

    return (
        <div className={styles.wrapper} >

            <Container>
                <Row>
                    <Col></Col>
                    <Col><Button onClick={() => applyFiltersAndSort()}>Apply Filters</Button></Col>
                    <Col><Button onClick={() => removeFiltersAndSort()}>No Filters</Button></Col>
                </Row>
                <Row>
                    <Col>Category</Col>
                    <Col>Lanugage</Col>
                    <Col>Published At</Col>
                    <Col>Country</Col>
                    <Col>Source</Col>
                    <Col>Sort</Col>
                </Row>
                <Row>

                    <Col>
                        <Filterable list={category_ranges} name={`category`} onFilter={addFilter}></Filterable>
                    </Col>
                    <Col>
                        <Filterable list={language_ranges} name={`language`} onFilter={addFilter}></Filterable>
                    </Col>
                    <Col>
                        <Filterable list={published_ranges} name={`published_at`} onFilter={addFilter}></Filterable>
                    </Col>
                    <Col>
                        <Filterable list={country_ranges} name={`country_id`} onFilter={addFilter}></Filterable>
                    </Col>
                    <Col>
                        <Filterable list={sources_ranges} name={`source`} onFilter={addFilter}></Filterable>
                    </Col>
                    <Col>
                        <Sortable dict={sort_list} onSort={addFilter}></Sortable>
                    </Col>
                </Row>
            </Container>

            {table}

        </div>
    );
}

NewsTable.defaultProps = {
    url: "https://api.findahome.me/news"
}

function News() {
    const navigate = useNavigate();

    const [searchParams, setSearchParams] = useSearchParams();

    let search = searchParams.get("q");
    let params = { search: search };

    function onSearch(query) {
        if (query) {
            navigate({
                pathname: "/news",
                search: `?${createSearchParams({ q: query })}`
            });
        } else {
            navigate("/news");
        }
    }

    return (
        <div className={styles.wrapper} >
            <h1><strong>News</strong></h1>
            <Searchbar onSearch={onSearch} value={search} />
            <NewsTable params={params} />
        </div>
    );

}

export default News;