import React from "react";
import { Card } from "antd"
import card_styles from "../InstanceCard.module.css";
import styles from "../Instance.module.css";
import {
    Row,
    Col,
} from 'react-bootstrap'

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';

class NewsInstance extends React.Component {
    render() {
        const news = this.props.news;

        let make_title = (id) => {
            const words = id.split("-");
            for (let i = 0; i < words.length; i++) {
                words[i] = words[i][0].toUpperCase() + words[i].substr(1) + " ";
            }
            return(words);
        }

        const pagination_charity = paginationFactory({
            page: 1,
            sizePerPage: 5,
            lastPageText: '>>',
            firstPageText: '<<',
            nextPageText: '>',
            prePageText: '<',
            alwaysShowAllBtns: true,
            hideSizePerPage: true
        });

        const charity_columns = [
            {
                dataField: 'name',
                text: 'Charity Names',
                formatter: (cell) => (
                    <a href={`/charity/${cell}`}> {make_title(cell)} </a>
                )
            }
        ]

        const charities = this.props.news.charities;
        let charity_ids = Array(charities.length);
        for(let i = 0; i < charities.length; i++){
            charity_ids[i] = {
                name: charities[i].id
            }
        }
        let charity_table;
        if(charity_ids.length === 0){
            charity_table = <h4><strong>No Relavent Charities</strong></h4>
        }
        else{
            charity_table = <BootstrapTable bootstrap4 keyField='name' data={charity_ids} columns={charity_columns} pagination={pagination_charity} />;
        }

        const country = this.props.news.country_id;

        let country_link = <h4><strong>General Information: </strong>N/A</h4>
        if (country != null){
            let link = "/country/" + country;
            country_link = <h4><strong>Learn More About: </strong><a href={link}>{country}</a></h4>
        }

        
        return (
            
           <div className={styles.wrapper}>
                    <h1 className={styles.title}>{`${news.title}`}</h1>
                    <Row>
                        <Card bordered={true} className={card_styles.card}>
                            <p> </p>
                            <h3 className={styles.description}><strong>Description: </strong></h3>
                            <h4 className={styles.description}>{news.description}</h4>
                            <p> </p>
                        </Card>
                    </Row>
                    <Row>
                            <p></p>
                    </Row>
                    <Row>
                            <Col>
                                <Card bordered={true} cover ={<img src={news.image} alt={"No Img Available"} className={card_styles.news_image}/>} 
                                className={card_styles.card} bodyStyle={{
                                    alignItems: "stretch",
                                    height: "100%",
                                    display: "flex",
                                    flexDirection: "column",
                                }}>
                                <Row>
                                    <Col><h4><strong>Learn More At: </strong></h4></Col>
                                </Row>
                                <Row>
                                    <h6><a href={`${news.url}`}>{news.url}</a></h6>
                                </Row>
                                </Card>
                            </Col>
                            <Col>
                            <Card bordered={true} className={card_styles.card}>
                                <Row>
                                    <Col><h4><strong>Language(s): </strong>{news.language}</h4></Col>
                                </Row>
                                <Row>
                                    <Col><h4><strong>Country: </strong>{country}</h4></Col>
                                </Row>
                                <Row>
                                    <Col><h4><strong>Author: </strong>{news.author}</h4></Col>
                                </Row>
                                <Row>
                                    <Col><h4><strong>Source: </strong>{news.source}</h4></Col>
                                </Row>
                                <Row>
                                    <Col><h4><strong>Category: </strong>{news.category}</h4></Col>
                                </Row>
                                <Row>
                                    <Col><h4><strong>Date Published: </strong>{news.published_at}</h4></Col>
                                </Row>
                            </Card>
                            </Col>
                        </Row>
                        <Row>
                            <p> </p>
                            <p> </p>
                        </Row>
                        <Row>
                            <h2 className={styles.subtitle}><strong>Are you thinking about moving to {country}?</strong></h2>
                        </Row>    
                        <Row>
                            <p> </p>
                        </Row>
                        <Row>
                            <Col><Card bordered={true} className={card_styles.card}>{country_link}</Card></Col>
                            <Col><Card bordered={true} className={card_styles.card}>
                                    <Row><h4><strong>Relevant Charities: </strong></h4></Row>
                                    <Row>{charity_table}</Row>
                            </Card></Col>
                        </Row>
                        <Row>
                            <p> </p>
                            <p></p>
                        </Row>            
            </div >
        );
    }
}

export default NewsInstance;