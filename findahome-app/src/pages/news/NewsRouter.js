import React from 'react';
import { useParams } from 'react-router-dom';
import { useFetch } from "../../App";
import NewsInstance from './NewsInstance';
import styles from "../../Home.module.css";
import {Row, Container } from 'react-bootstrap';
import Loader from "../../components/Loader";

function NewsRouter() {
  let { id } = useParams();
  const [newsData, loading] = useFetch(`https://api.findahome.me/news/${id}`);
  if (loading || newsData == null) {
    return (
      <div className={styles.wrapper}>
          <p> </p>
          <p> </p>
          <Container className={styles.flexbox}>
              <Row>
                  <Loader/>
              </Row>     
          </Container>
          <p> </p>
          <p> </p> 
      </div>
    );
  } 
  return <NewsInstance news={newsData} />;
}

export default NewsRouter;
