//Code Adapted from: https://www.geeksforgeeks.org/create-a-radar-chart-using-recharts-in-reactjs/
import Loader from '../../components/Loader';
import {
    Row,
    Container,
} from 'react-bootstrap'
import { useFetch } from "../../App";
import {PieChart, Cell, Pie, ResponsiveContainer, Tooltip, Legend } from 'recharts';
import styles from "../Visualizations.module.css"

const ActiveChart = () => {
    //Get Highest Funded Charities
    const COLORS = ["#012359", "#033482", "#055ae3", "#2e7eff", "#4188fa", 
    "#00aeed", "#513EA4", "#188fb8","#000000", "#62cff5", "#B5B5B5", "#06218F", "#EDEDED",
    "#7F898F", "#748FFC", "#30678C", "#5A7B91", "#101838", "#0231ED", "#000000", "#FFFFFF",
    "#373DD1", "#3F3FD8"];
    let [charityData, loading] = useFetch("https://api.findahome.me/charities?per_page=100&page=1&sort=funding-dec");

    if (loading) {
        return(
            <Container className={styles.flexbox}>
            <Row>
                <Loader/>
            </Row>     
            </Container>
        )
    }
    if (loading) {
        return(
          <Container className={styles.flexbox}>
          <Row>
              <Loader/>
          </Row>     
          </Container>
        )
    }
    else {
        let data = charityData.result
        let curThemeMap = new Map()
        for(let i = 0; i < data.length; i++){
            let themes = data[i].themes;
            for(let r = 0; r < themes.length; r++){
                let theme = themes[r];
                if(curThemeMap.has(theme.name)){
                    curThemeMap.set(theme.name, curThemeMap.get(theme.name) + 1);
                }
                else {
                    curThemeMap.set(theme.name, 1);
                }
            }
        }


        let parsed = new Array(curThemeMap.size);
        let i = 0;
        for (let [key, value] of curThemeMap){
            parsed[i] = {
                "Theme": key,
                "Number": value
            }
            i++
        }


        const renderLegendText = (value) => {
            let theme = parsed[value].Theme + ": " + parsed[value].Number;
            return <span>{theme}</span>;
        };

        const renderTooltipText = (name, value, entry) => {
            let tip = parsed[value].Theme;
            let quant = ": " + name + " Charities"
            return <span><strong>{tip}</strong>{quant}</span>;
        };

        return (
            <ResponsiveContainer width="100%" height={500}>
                <PieChart>
    
                <Pie data={parsed} dataKey="Number">
                    {data.map((entry, index) => (
                        <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                    ))}
                </Pie>
                <Tooltip
                    contentStyle={{backgroundColor: "#012359"}}
                    labelStyle={{ color: "#188fb8" }}
                    formatter={renderTooltipText}
                />
        
                <Legend formatter={renderLegendText} iconSize={15} wrapperStyle={{fontSize: "15px"}}/>
                </PieChart>
            </ResponsiveContainer>
        );
    }
}

export default ActiveChart;