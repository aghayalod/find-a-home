//Code Adapted from: https://www.geeksforgeeks.org/create-a-radar-chart-using-recharts-in-reactjs/
import React from "react";
import Loader from '../../components/Loader';
import {Row,Container} from 'react-bootstrap';
import { useFetch } from "../../App";
import styles from "../Visualizations.module.css"
import {BarChart, Bar, XAxis, YAxis, ResponsiveContainer, Tooltip, Legend } from 'recharts';


const LanguageChart = () => {
    //Get Top 100 songs
    let [countryData, loading] = useFetch("https://api.findahome.me/countries?per_page=41");

    if (loading) {
        return(
            <Container className={styles.flexbox}>
            <Row>
                <Loader/>
            </Row>     
            </Container>
        )
    }

    let data = countryData.result;

    // Generating Genre Map of top 100 songs 
    let languageMap = new Map()
    for(let i = 0; i < data.length; i++){
        let lang = data[i].language;
        if(lang != null){
            if(languageMap.has(lang)){
                const arr = languageMap.get(lang);
                arr.push(data[i].name)
                languageMap.set(lang, arr);
            }
            else {
                languageMap.set(lang, [data[i].name]);
            }
        }
    }

    //Parsing genreMap into list
    let parsed = new Array(languageMap.size);
    let i = 0;
    for (let [key, value] of languageMap){
        parsed[i] = {
            "Language": key,
            "Number of Countries": value.length
        }
        i++
    }
    return (
        <ResponsiveContainer width="100%" height={900}>
    	<BarChart 
            data={parsed}
            layout="vertical" barCategoryGap={1}
            margin={{ top: 0, right: 50, left: 0, bottom: 0 }}>
            <XAxis type="number" />
            <Tooltip
                contentStyle={{backgroundColor: "#012359"}}
                labelStyle={{ color: "#188fb8" }}
                labelFormatter={function(value) {
                return `Countries that speak ${value}: ${languageMap.get(value)}`;
                }}
            />
            <Legend />
            <YAxis type="category" width={200} padding={{ left: 20 }} dataKey="Language"/>
            <Bar dataKey="Number of Countries" fill="#188fb8" />   
        </BarChart>
     </ResponsiveContainer>
  
        );
 
}

export default LanguageChart;