//Code Adapted from: https://www.react-simple-maps.io/examples/map-chart-with-tooltip/
//                   https://www.react-simple-maps.io/examples/world-choropleth-mapchart/
import React, { memo } from "react";
import Loader from '../../components/Loader';
import {Row,Container} from 'react-bootstrap';
import { useFetch } from "../../App";
import styles from "../Visualizations.module.css"
import { scaleLinear } from "d3-scale";
import {
  ComposableMap,
  Geographies,
  Geography,
  Sphere,
  Graticule,
  ZoomableGroup,
} from "react-simple-maps";


const geoUrl =
  "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json";

const colorScale = scaleLinear()
  .domain([4.9, 8.0])
  .range(["#52bdff", "#020ed4"]);

const MapChart = ({setTooltipContent}) => {
  let [countryData, loading] = useFetch("https://api.findahome.me/countries?per_page=41");

  if (loading) {
      return(
        <Container className={styles.flexbox}>
        <Row>
            <Loader/>
        </Row>     
        </Container>
      )
  }

  let data = countryData.result;

  return (
    <ComposableMap data-tip="" width={730} height={330} projectionConfig={{ scale: 125 }}>
        <ZoomableGroup>
        <Sphere stroke="#E4E5E6" strokeWidth={0.5} />
        <Graticule stroke="#E4E5E6" strokeWidth={0.5} />
        {data.length > 0 && (
        <Geographies geography={geoUrl}>
            {({ geographies }) =>
            geographies.map((geo) => {
                const d = data.find((s) => s.id === geo.properties.ISO_A3);
                return (
                <Geography
                    key={geo.rsmKey}
                    geography={geo}
                    onMouseEnter={() => {
                        const { NAME } = geo.properties;
                        const rating = d ? d["life_satisfaction_rating"] : "N/A";
                        setTooltipContent(`${NAME}: ${rating}`);
                      }}
                      onMouseLeave={() => {
                        setTooltipContent("");
                      }}
                      style={{
                        default: {
                            fill: d ? colorScale(d["life_satisfaction_rating"]) : "#F5F4F6",
                            outline: "none"
                        },
                        hover: {
                          fill: "#a56ae6",
                          outline: "none"
                        },
                        pressed: {
                          fill: "#E42",
                          outline: "none"
                        }
                      }}
                />
                );
            })
            }
        </Geographies>
        )}
        </ZoomableGroup>
    </ComposableMap>
  );
};
export default memo(MapChart);
