import {Row, Container} from 'react-bootstrap'
import styles from "../Visualizations.module.css"
import React, { useState } from "react";
import MapChart from "./MapChart";
import LanguageChart from "./LanguageChart"
import ActiveChart from "./ActiveChart"
import ReactTooltip from "react-tooltip";


export default function OurVisualization (){
        const [content, setContent] = useState("");
        return (
            <div className={styles.wrapper}>
                <Container className={styles.flexbox}>
                    <Row><h1><strong>Average Life Satisfaction Raitings</strong></h1></Row>
                    <Row><h4>[hover over each country to find out more]</h4></Row>
                </Container>
                    <MapChart setTooltipContent={setContent} />
                    <ReactTooltip style={{width: "100px"}}>{content}</ReactTooltip>
                    <h2>Average Life Rating in {content}</h2>
                <Container className={styles.flexbox}>
                    <Row><h1><strong>Language Distribution</strong></h1></Row>
                </Container>
                <Row><LanguageChart/></Row>   
                <Container className={styles.flexbox}>
                    <Row><h1><strong>Themes of the Most Funded Charities</strong></h1></Row>
                    <Row><h4>[hover over each section to find out more]</h4></Row>
                </Container>
                <Row><ActiveChart/></Row>  
            </div>
        );
  }