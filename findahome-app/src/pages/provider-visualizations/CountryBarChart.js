//Code adapted from: https://www.geeksforgeeks.org/create-a-bar-chart-using-recharts-in-reactjs/

import React from 'react';
import Loader from '../../components/Loader';
import {Row,Container} from 'react-bootstrap';
import { useFetch } from "../../App";
import {BarChart, Bar, CartesianGrid, XAxis, YAxis, ResponsiveContainer, Tooltip, Legend } from 'recharts';
import styles from "../Visualizations.module.css"

function CountryBarChart() {
  const [countryData, loading] = useFetch(`https://api.lyriccheck.me/countries?sort=-profanity_average&page[number]=4`);
  const [guyanaData, gLoading] = useFetch(`https://api.lyriccheck.me/countries/GY`);
    
  if (loading || gLoading) {
    return(
      <Container className={styles.flexbox}>
      <Row>
          <Loader/>
      </Row>     
      </Container>
    )
  }
  else {
    let parsed = new Array(11);
    let gy = guyanaData.data.attributes;
    parsed[0] = {
        "Country Name": gy.name_common,
        "Profanity Average": gy.profanity_average,
    };
    for(let i = 1; i < 11; i++){
      let cur = countryData.data[i-1].attributes;
      parsed[i] = {
        "Country Name": cur.name_common,
        "Profanity Average": cur.profanity_average,
      };
    }
    return (
      <ResponsiveContainer width="100%" height={600}>
        <BarChart
          width={700}
          height={600}
          data={parsed}
          margin={{ top: 10, right: 50, left: 50, bottom: 50 }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="Country Name"></XAxis>
          <YAxis />
          <Tooltip
            contentStyle={{backgroundColor: "#012359"}}
            labelStyle={{ color: "#188fb8" }}
            labelFormatter={function(value) {
              return `Country: ${value}`;
            }}
          />
          <Legend />
          <Bar dataKey="Profanity Average" fill="#188fb8" />
        </BarChart>
      </ResponsiveContainer>

      );
  }
}

export default CountryBarChart;
