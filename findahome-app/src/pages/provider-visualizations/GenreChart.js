//Code Adapted from: https://www.geeksforgeeks.org/create-a-radar-chart-using-recharts-in-reactjs/

import Loader from '../../components/Loader';
import { useFetch } from "../../App";
import {ResponsiveContainer, Radar, RadarChart, PolarGrid, 
  PolarAngleAxis, PolarRadiusAxis } from 'recharts';
import {
  Row,
  Container,
} from 'react-bootstrap'
import styles from "../Visualizations.module.css"

const GenreChart = () => {
  //Get Top 100 songs
  let url = "https://api.lyriccheck.me/songs?sort=-views&page[size]=100&page[number]=1";
  let [songData, loading] = useFetch(url);
  if (loading) {
    return(
      <Container className={styles.flexbox}>
      <Row>
          <Loader/>
      </Row>     
      </Container>
    )
  }

  else{
    // Generating Genre Map of top 100 songs 
    let genreMap = new Map()
    let songs = songData.data;
    for(let i = 0; i < songs.length; i++){
      let cur = songs[i].attributes;
      let genre = cur.genre;
      if(genre != null){
        if(genreMap.has(genre)){
          genreMap.set(genre, genreMap.get(genre) + 1);
        }
        else {
          genreMap.set(genre, 1);
        }
      }
    }

    //Parsing genreMap into list
    let parsed = new Array(genreMap.size);
    let i = 0;
    for (let [key, value] of genreMap){
      parsed[i] = {
        "Genre": key,
        "Number of Songs": value
      }
      i++
    }
    return (
      <ResponsiveContainer width="100%" height={600}>
        <RadarChart
          width={700}
          height={600}
          data={parsed}
          color={"#188fb8"}
          margin={{ top: 10, right: 50, left: 50, bottom: 50 }}
        >
          <PolarGrid />
          <PolarAngleAxis dataKey="Genre" stroke="#055ae3"/>
          <PolarRadiusAxis />
          <Radar dataKey="Number of Songs" stroke="#055ae3" 
              fill="#188fb8" fillOpacity={0.75} />
      </RadarChart>
      </ResponsiveContainer>
  );
  } 
}

export default GenreChart;