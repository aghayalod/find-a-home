//Code Adapted from: https://www.react-simple-maps.io/examples/map-chart-with-tooltip/
//                   https://www.react-simple-maps.io/examples/world-choropleth-mapchart/
import React, {memo} from "react";
import {
  ComposableMap,
  Geographies,
  Geography,
  Sphere,
  Graticule,
  ZoomableGroup,
} from "react-simple-maps";
import { useFetch } from "../../App";
import Loader from '../../components/Loader';
import {
    Row,
    Container,
  } from 'react-bootstrap'
  import styles from "../Visualizations.module.css"

const geoUrl =
  "https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json";

const MostPopularMap = ({setTooltipContent}) => {
    //Getting current most popular song
    let url = "https://api.lyriccheck.me/songs?sort=-views&page[size]=1&page[number]=1";
    let [popularSong, loading] = useFetch(url); 
    if (loading) {
        return(
            <Container className={styles.flexbox}>
            <Row>
                <Loader/>
            </Row>     
            </Container>
          )
    }
    else{
        let song = popularSong.data[0].attributes;
        let data = song.countries_popular_in;
        
        return (
            <ComposableMap data-tip="" width={730} height={330} projectionConfig={{ scale: 125 }}>
                <ZoomableGroup>
                <Sphere stroke="#E4E5E6" strokeWidth={0.5} />
                <Graticule stroke="#E4E5E6" strokeWidth={0.5} />
                {data.length > 0 && (
                <Geographies geography={geoUrl}>
                    {({ geographies }) =>
                    geographies.map((geo) => {
                        const d = data.find((s) => s === geo.properties.ISO_A2);
                        return (
                        <Geography
                            key={geo.rsmKey}
                            geography={geo}
                            onMouseEnter={() => {
                                const { NAME } = geo.properties;
                                const cont = d ? song.name + " IS popular in " + NAME: song.name + " IS NOT popular in " + NAME;
                                setTooltipContent(cont);
                              }}
                              onMouseLeave={() => {
                                setTooltipContent("");
                              }}
                            style={{
                                default: {
                                    fill: d ? "#00aeed":"#F5F4F6",
                                    stroke: "black"
                                },
                                hover: {
                                fill: "#a56ae6",
                                outline: "none"
                                },
                                pressed: {
                                fill: "#E42",
                                outline: "none"
                                }
                            }}
                        />
                        );
                    })
                    }
                </Geographies>
                )}
                </ZoomableGroup>
            </ComposableMap>
        );
    }
};
export default memo(MostPopularMap);
