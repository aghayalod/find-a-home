import CountryBarChart from "./CountryBarChart";
import GenreChart from "./GenreChart";
import MostPopularMap from "./MostPopularMap"
import {
    Row,
    Container,
  } from 'react-bootstrap'
import styles from "../Visualizations.module.css"
import React, { useState } from "react";
import ReactTooltip from "react-tooltip";


export default function ProviderVisualizations(){
    const [content, setContent] = useState("");
    return (
        <div className={styles.wrapper}>
            <Container className={styles.flexbox}>
                <Row><h1><strong>Countries With the Highest Profanity Ratings</strong></h1></Row>
            </Container>
            <Row><CountryBarChart/></Row>

            <Container className={styles.flexbox}>
                <Row><h1><strong>Genre Distribution of the Top 100 Songs</strong></h1></Row>
                <Row><h4>[number of songs per genre]</h4></Row>
            </Container>
            <Row><GenreChart/></Row>
            
            <Container className={styles.flexbox}>
                <Row><h1><strong>Popularity Distribution of the Most Popular Song</strong></h1></Row>
                <Row><h4>[hover over each country to find out more]</h4></Row>
            </Container>
                <MostPopularMap setTooltipContent={setContent}/>
                <ReactTooltip style={{width: "100px"}}>{content}</ReactTooltip>
                <h1><strong>Status: </strong>{content}</h1>
            
        </div>
    );
}