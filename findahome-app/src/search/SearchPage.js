import React, {useState} from "react";
import { useNavigate, createSearchParams, useSearchParams} from "react-router-dom";
import Searchbar from "./Searchbar";
import { useFetch } from "../App";
import { CharityTable } from "../pages/models/Charity";
import { CountryTable } from "../pages/models/Country";
import { NewsTable } from "../pages/models/News";
import styles from "../Home.module.css"

export function SearchPage(props) {
    const navigate = useNavigate();
    const [searchParams, setSearchParams] = useSearchParams();

    let search = searchParams.get("q");
    let params = {search: search};

    let countryUrl = `https://api.findahome.me/countries`
    let charityUrl = `https://api.findahome.me/charities`
    let newsUrl = `https://api.findahome.me/news`

    function onSearch(query) {
        if(!query) {
            navigate("/search");
            return;
        }
        navigate({
            pathname: "/search",
            search: `?${createSearchParams({ q: query})}`
        });
    }

    let results = null;
    let countries = null;
    let news = null;
    let charities = null;
    if(search) {
        countries = <CountryTable url={countryUrl} params={params}/>;
        news = <NewsTable url={newsUrl} params={params}/>;
        charities = <CharityTable url={charityUrl} params={params}/>;
        results = (
            <div className={styles.wrapper}>
            <React.Fragment>
                <h1 className={styles.title}><strong>Countries</strong></h1>
                {countries}
                <h1 className={styles.title}><strong>News</strong></h1>
                {news}
                <h1 className={styles.title}><strong>Charities</strong></h1>
                {charities}
            </React.Fragment>
            </div>
        );
    } else {
        results = <div className={styles.wrapper}><h1 className={styles.title}><strong>Search our entire website!</strong></h1></div>;
    }

    return (
        <div className={styles.wrapper}>
        <React.Fragment>
            <Searchbar onSearch={onSearch} value={search} />
            {results}
        </React.Fragment>
        </div>
    );
}