import React, { useState } from "react"
import { AutoComplete, Input } from "antd"
import styles from "../Home.module.css"

const { Search } = Input

export default function Searchbar({
	autoComplete,
	autoCompleteOptions,
	clear,
	value,
	placeholder,
	onSearch,
	onChange,
	onOptionSelect,
}) {

	const [text, setText] = useState(value);

	const onSearchChange = (e) => {
		setText(e.target.value);
		if(onChange) {
			onChange(e.target.value)
		}
	}

	let props = {
		size: "large",
		enterButton: "Search",
		placeholder: placeholder || "Enter sitewide search",
		onChange: onSearchChange,
		onSearch: onSearch,
		value: text,
		allowClear: true,
		bordered: true,
	}

	if (clear) {
		props.bordered = false
	}


	return (
			<div className={styles.search_container}>
				<Search {...props} />
			</div>
		)
}


