FRONTEND_DIR := findahome-app
BACKEND_DIR := backend

docker-frontend:
	$(MAKE) -C $(FRONTEND_DIR) docker

docker-backend:
	$(MAKE) -C $(BACKEND_DIR) docker

build:
	$(MAKE) -C $(FRONTEND_DIR) build
	$(MAKE) -C $(BACKEND_DIR) build

clean:
	$(MAKE) -C $(FRONTEND_DIR) clean
	$(MAKE) -C $(BACKEND_DIR) clean

format:
	black findahome-app
	black backend

run-selenium:
	wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
    sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
    apt-get -y update
    apt-get install -y google-chrome-stable

    # Download and unzip the chromedriver
    apt-get install -yqq unzip
    wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip
    unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/
    python3 ./findahome-app/selenium/guitests.py